import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom'
import RecipeTilesScreen from "../../components/RecipeTilesScreen/RecipeTilesScreen";
import RecipeScreenContainer from "../RecipeScreenContainer/RecipeScreenContainer";
import UserScreenContainer from '../UserScreenContainer/UserScreenContainer';

class MainScreen extends Component {
    render() {
        return (
            <div>
                <Switch>
                    <Route exact path="/" component={RecipeTilesScreen} />
                    <Route path="/recipe/:recipeId" component={RecipeScreenContainer} />
                    <Route path="/user" component={UserScreenContainer} />
                    <Redirect to="/" />
                </Switch>
            </div>
        );
    }
}

export default MainScreen;