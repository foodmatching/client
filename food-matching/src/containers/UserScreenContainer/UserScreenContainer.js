import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import UserScreen from '../../components/UserScreen/UserScreen';

class UserScreenContainer extends Component {
    render() {
        let screen = <Redirect to='/' />;

        if (this.props.user) {
            screen = <UserScreen />
        }

        return screen;
    }

}

const mapStateToProps = state => {
    return {
        user: state.users.user
    }
}
export default connect(mapStateToProps)(UserScreenContainer);
