import React, { Component } from "react";
import RecipeScreen from "../../components/RecipeScreen/RecipeScreen";
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import { server } from '../../Utils/utils';


class RecipeScreenContainer extends Component {
    render() {
        let screen = <Redirect to='/' />;

        if (this.props.recipes) {
            const recipe = this.props.recipes.find(recipe => recipe._id === this.props.match.params.recipeId);
            if (recipe) {
                screen = <RecipeScreen {...recipe} />;
            } else {
                axios.post(server.recipeRequests.GetRecipeByID, { id: this.props.match.params.recipeId })
                    .then(response => {
                        console.log(response)
                        screen = <RecipeScreen {...response.data} />;
                    }).catch(console.log)
            }

        }

        return screen;
    }
}

const mapStateToProps = state => {
    return {
        recipes: state.recipes.recipeList
    }
}

export default connect(mapStateToProps)(RecipeScreenContainer);