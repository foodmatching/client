import * as actionTypes from './actionTypes';
import axios from 'axios';
import { server, updateObject } from '../../Utils/utils';

export const showRegistrationModal = () => {
    return {
        type: actionTypes.SHOW_REGISTRATION_MODAL
    }
}

export const hideRegistrationModal = () => {
    return {
        type: actionTypes.HIDE_REGISTRATION_MODAL
    }
}

export const showLoginModal = () => {
    return {
        type: actionTypes.SHOW_LOGIN_MODAL
    }
}

export const hideLoginModal = () => {
    return {
        type: actionTypes.HIDE_LOGIN_MODAL
    }
}

export const authenticationStart = (username, password) => {
    return dispatch => {
        axios.post(server.userRequests.connect, { username: username, password: password })
            .then(async () => {
                await axios.post(server.userRequests.getUserByUserName, { username: username })
                    .then(res => {
                        const healthIssues = [...res.data.profile.healthIssues];
                        const ingredientsIDsLikes = [...res.data.profile.ingredientsIDsLikes];
                        const typesLikes = [...res.data.profile.typesLikes];
                        const profile = updateObject(res.data.profile, {
                            healthIssues: healthIssues,
                            ingredientsIDsLikes: ingredientsIDsLikes,
                            typesLikes: typesLikes
                        });

                        let cookBooks = [];
                        res.data.cookBooks.forEach(cb => {
                            const recipesIds = [...cb.recipes]
                            const cookBook = updateObject(cb, { recipesIds: recipesIds });
                            cookBooks.push(cookBook);
                        });
                        const user = updateObject(res.data, {
                            profile: profile,
                            cookBooks: cookBooks
                        });
                        dispatch(authenticationSuccess(user))
                    })
                    .catch((err) => {
                        dispatch(authenticationFail());
                    });
            })
            .catch(error => {
                dispatch(authenticationFail());
            });
    }
}

const authenticationSuccess = (user) => {
    return {
        type: actionTypes.AUTHENTICATION_SUCCESS,
        user: user
    }
}

const authenticationFail = () => {
    return {
        type: actionTypes.AUTHENTICATION_FAIL
    }
}

export const addNewCookBook = (userID, cookBookName) => {
    return dispatch => {
        axios.post(server.userRequests.addCookBook, { id: userID, name: cookBookName })
            .then(async response => {
                await axios.post(server.userRequests.getAllCookBooks, { id: userID })
                    .then(response => {
                        dispatch(addNewCookBookSuccess(response.data));
                    })
                    .catch(() => dispatch(addNewCookBookFail('request from server failed')));
            })
            .catch(() => dispatch(addNewCookBookFail('request from server failed')));
    }
}

const addNewCookBookSuccess = (cookBooks) => {
    return {
        type: actionTypes.ADD_BOOK_SUCCESS,
        cookBooks: cookBooks
    }
}

const addNewCookBookFail = (error) => {
    return {
        type: actionTypes.ADD_BOOK_FAIL,
        error: error
    }
}

export const authenticationReset = () => {
    return {
        type: actionTypes.AUTHENTICATION_RESET
    }
}

export const addRecipeToCookBook = (userID, cookBookName, recipeID) => {
    return dispatch => {
        axios.post(server.userRequests.addRecipeToCookBook, { id: userID, name: cookBookName, recipeId: recipeID })
            .then(async response => {
                //need to update user
                await axios.post(server.userRequests.getAllCookBooks, { id: userID })
                    .then(response => {
                        dispatch(addRecipeToCookBookSuccess(response.data));
                    })
                    .catch(() => dispatch(addRecipeToCookBookFail('Server Error: Recipe Saved, but couldn\'t reload user')));
            })
            .catch(() => dispatch(addRecipeToCookBookFail('Server Error: Couldn\'t save the recipe.')));
    }
}

const addRecipeToCookBookSuccess = cookBooks => {
    return {
        type: actionTypes.ADD_RECIPE_TO_BOOK_SUCCESS,
        cookBooks: cookBooks
    }
}

const addRecipeToCookBookFail = error => {
    return {
        type: actionTypes.ADD_RECIPE_TO_BOOK_FAIL,
        error: error
    }
}

export const logout = () => {
    return {
        type: actionTypes.LOGOUT
    }
}