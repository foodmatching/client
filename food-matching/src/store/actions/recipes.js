import * as actionTypes from './actionTypes';
import axios from 'axios';
import { server } from '../../Utils/utils';

export const initRecipeList = (page, size, userID, personalized) => {
    return dispatch => {
        dispatch(setRecipeListStart());
        let request = server.recipeRequests.getAllRecipes;
        let body = null;
        if (userID) {
            body = { ...body, id: userID };
            request = personalized ? server.userRequests.getRecipesMatchForUser : server.userRequests.getAllUserRecipes;
            // request = server.userRequests.getAllUserRecipes;
        }
        if (page && size) {
            body = { ...body, pageNo: page, size: size }
        }
        axios.post(request, body)
            .then(async (response) => {
                let recipeList = [];
                if (userID && !personalized) {
                    response.data.forEach(el => recipeList.push(el.recipe));
                } else {
                    recipeList = response.data;
                }
                dispatch(setRecipeListSuccess(recipeList));
            })
            .catch(error => {
                dispatch(fetchRecipeListFail(error));
            });
    }
}

export const resetRecipeList = () => {
    return {
        type: actionTypes.RESET_RECIPE_LIST
    }
}

const setRecipeListStart = () => {
    return {
        type: actionTypes.SET_RECIPE_LIST_START,
        loading: true
    }
}

const setRecipeListSuccess = recipes => {
    return {
        type: actionTypes.SET_RECIPE_LIST,
        recipeList: recipes
    }
}

const fetchRecipeListFail = error => {
    return {
        type: actionTypes.FETCH_RECIPE_LIST_FAIL,
        error: error
    }
}

export const addCommentToRecipe = (recipeID, comment, picture) => {
    return dispatch => {
        dispatch(addCommentToRecipeStart);
        axios.post(server.recipeRequests.addComment, { id: recipeID, comment: comment })
            .then(async response => {
                //succeeded to add comment:
                //add picture to the comment
                // const commentID = response.data;

                //get the updated recipe
                await axios.post(server.recipeRequests.GetRecipeByID, { id: recipeID })
                    .then(async res => dispatch(addCommentToRecipeSuccess(res.data)))
                    .catch(() => dispatch(addCommentToRecipeFail('new comment added but recipe could not be reloaded')));

            })
            .catch(error => {
                dispatch(addCommentToRecipeFail(error))
            });
    }
}

const addCommentToRecipeStart = () => {
    return {
        type: actionTypes.ADD_COMMENT_TO_RECIPE_START,
        addCommentLoading: true
    }
}

const addCommentToRecipeFail = (error) => {
    return {
        type: actionTypes.ADD_COMMENT_TO_RECIPE_FAIL,
        addCommentLoading: false,
        error: error
    }
}

const addCommentToRecipeSuccess = (recipe) => {
    return {
        type: actionTypes.ADD_COMMENT_TO_RECIPE_SUCCESS,
        addCommentLoading: false,
        recipe
    }
}

export const showNewRecipeModal = () => {
    return {
        type: actionTypes.SHOW_NEW_RECIPE_MODAL
    }
}

export const hideNewRecipeModal = () => {
    return {
        type: actionTypes.HIDE_NEW_RECIPE_MODAL
    }
}

export const addNewRecipe = (recipe, picture) => {
    return dispatch => {
        dispatch(addNewRecipeStart())
        axios.post(server.recipeRequests.addRecipe, { recipe: recipe })
            .then(async response => {
                //upload picture
                console.log(response)
                // await axios.post(server.recipeRequests.addRecipePicture, { id: response.data, image: picture })
                //     .then(console.log).catch(console.log)
                dispatch(addNewRecipeSuccess())
            })
            .catch((err) => dispatch(addNewRecipeFail()));
    }
}

const addNewRecipeStart = () => {
    return {
        type: actionTypes.ADD_NEW_RECIPE_START,
        loading: true
    }
}

const addNewRecipeSuccess = () => {
    return {
        type: actionTypes.ADD_NEW_RECIPE_SUCCESS
    }
}

const addNewRecipeFail = () => {
    return {
        type: actionTypes.ADD_NEW_RECIPE_FAIL,
        loading: false
    }
}