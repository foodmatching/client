/* ________________________________________Recipes basic "feed"________________________________________ */
export const FETCH_RECIPE_LIST_FAIL = 'FETCH_RECIPE_LIST_FAIL';
export const SET_RECIPE_LIST = 'SET_RECIPE_LIST';
export const SET_RECIPE_LIST_START = 'SET_RECIPE_LIST_START';
export const ADD_COMMENT_TO_RECIPE_START = 'ADD_COMMENT_TO_RECIPE_START';
export const ADD_COMMENT_TO_RECIPE_SUCCESS = 'ADD_COMMENT_TO_RECIPE_SUCCESS';
export const ADD_COMMENT_TO_RECIPE_FAIL = 'ADD_COMMENT_TO_RECIPE_FAIL';
export const RESET_RECIPE_LIST = 'RESET_RECIPE_LIST';

// Personalized "cook-books"
export const ADD_RECIPE_TO_BOOK_SUCCESS = 'ADD_RECIPE_TO_BOOK_SUCCESS';
export const ADD_RECIPE_TO_BOOK_FAIL = 'ADD_RECIPE_TO_BOOK_FAIL';
export const REMOVE_RECIPE_TO_BOOK = 'REMOVE_RECIPE_TO_BOOK';
export const ADD_BOOK_SUCCESS = 'ADD_BOOK_SUCCESS';
export const ADD_BOOK_FAIL = 'ADD_BOOK_FAIL';
export const REMOVE_BOOK = 'REMOVE_BOOK';

/* ________________________________________Users________________________________________ */
// Registration / Login / Logout
export const SHOW_REGISTRATION_MODAL = 'SHOW_REGISTRATION_MODAL';
export const HIDE_REGISTRATION_MODAL = 'HIDE_REGISTRATION_MODAL';
export const SHOW_LOGIN_MODAL = 'SHOW_LOGIN_MODAL';
export const HIDE_LOGIN_MODAL = 'HIDE_LOGIN_MODAL';
export const LOGOUT = 'LOGOUT'

// Authentication
export const AUTHENTICATION_START = 'AUTHENTICATION_START';
export const AUTHENTICATION_SUCCESS = 'AUTHENTICATION_SUCCESS';
export const AUTHENTICATION_FAIL = 'AUTHENTICATION_FAIL';
export const AUTHENTICATION_REDIRECT = 'AUTHENTICATION_REDIRECT';
export const AUTHENTICATION_RESET = 'AUTHENTICATION_RESET';
// TODO: getSpecificUserByFacebook,getSpecificUserByGmail

// Actions
export const SHOW_NEW_RECIPE_MODAL = 'SHOW_NEW_RECIPE_MODAL';
export const HIDE_NEW_RECIPE_MODAL = 'HIDE_NEW_RECIPE_MODAL';
export const ADD_NEW_RECIPE_START = 'ADD_NEW_RECIPE_START';
export const ADD_NEW_RECIPE_SUCCESS = 'ADD_NEW_RECIPE_SUCCESS';
export const ADD_NEW_RECIPE_FAIL = 'ADD_NEW_RECIPE_FAIL';
// TODO: deleteUserById, addUserPicture, addCookBook, addRecipeToCookBook, getAllCookBooks, getAllRecipesInCookBook
// TODO: getRecipesMatchForUser, isRecipeRecommendedForUser, isIngredientRecommendedForUser, addTypesLikes, addIngredientsIDsLikes, addHealthIssues


/* ________________________________________Comments________________________________________*/