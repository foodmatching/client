import { updateObject, deepCloneObject } from '../../Utils/utils';
import * as actionTypes from '../actions/actionTypes';

const initialState = {
    recipeList: null,
    loading: false,
    addCommentLoading: false,
    addNewRecipeLoading: false,
    showNewRecipeModal: false,
    newRecipeUploadSuccess: false
}

const fetchRecipeListFail = (state, action) => {
    return updateObject(state, { loading: false })
}

const setRecipeList = (state, action) => {
    return updateObject(state, { recipeList: action.recipeList, loading: false });
}

const addCommentToRecipeFail = (state, action) => {
    return updateObject(state, { error: action.error });
}

const updateRecipe = (state, action) => {
    const updatedRecipe = action.recipe;
    const updatedRecipeList = deepCloneObject(state.recipeList);//deep clone recipe list
    updatedRecipeList[updatedRecipeList.findIndex(el => el._id === updatedRecipe._id)] = updatedRecipe;
    return updateObject(state, { recipeList: updatedRecipeList, addCommentLoading: false });
}

const addRecipeSuccess = (state, action) => {
    return updateObject(state, { newRecipeUploadSuccess: true, addNewRecipeLoading: false });
}
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_RECIPE_LIST_START: return updateObject(state, { loading: true })
        case actionTypes.SET_RECIPE_LIST: return setRecipeList(state, action);
        case actionTypes.FETCH_RECIPE_LIST_FAIL: return fetchRecipeListFail(state, action);
        case actionTypes.ADD_COMMENT_TO_RECIPE_START: return updateObject(state, { addCommentLoading: true });
        case actionTypes.ADD_COMMENT_TO_RECIPE_FAIL: return addCommentToRecipeFail(state, action);
        case actionTypes.ADD_COMMENT_TO_RECIPE_SUCCESS: return updateRecipe(state, action);
        case actionTypes.SHOW_NEW_RECIPE_MODAL: return updateObject(state, { showNewRecipeModal: true, newRecipeUploadSuccess: false });
        case actionTypes.HIDE_NEW_RECIPE_MODAL: return updateObject(state, { showNewRecipeModal: false, newRecipeUploadSuccess: false });
        case actionTypes.ADD_NEW_RECIPE_START: return updateObject(state, { addNewRecipeLoading: true });
        case actionTypes.ADD_NEW_RECIPE_FAIL: return updateObject(state, { newRecipeUploadSuccess: false, addNewRecipeLoading: false });
        case actionTypes.ADD_NEW_RECIPE_SUCCESS: return addRecipeSuccess(state, action);
        case actionTypes.RESET_RECIPE_LIST: return updateObject(state, { recipeList: null })
        default: return state;
    }
}

export default reducer;