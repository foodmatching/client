import { updateObject, deepCloneObject } from '../../Utils/utils';
import * as actionTypes from '../actions/actionTypes';

const initialState = {
    user: null,
    isLoggedIn: false,
    showRegistration: false,
    showLogin: false,
    loginFailed: false,
    addBookFailed: false,
    addRecipeToBookFailed: false
}

const addBookSuccess = (state, action) => {
    const updatedUser = deepCloneObject(state.user);
    updatedUser.cookBooks = action.cookBooks;
    return updateObject(state, { user: updatedUser });
}


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SHOW_REGISTRATION_MODAL: return updateObject(state, { showRegistration: true, showLogin: false });
        case actionTypes.HIDE_REGISTRATION_MODAL: return updateObject(state, { showRegistration: false });
        case actionTypes.SHOW_LOGIN_MODAL: return updateObject(state, { showRegistration: false, showLogin: true });
        case actionTypes.HIDE_LOGIN_MODAL: return updateObject(state, { showLogin: false });
        case actionTypes.AUTHENTICATION_SUCCESS: return updateObject(state, { user: action.user, isLoggedIn: true, loginFailed: false });
        case actionTypes.AUTHENTICATION_FAIL: return updateObject(state, { loginFailed: true });
        case actionTypes.AUTHENTICATION_RESET: return updateObject(state, { loginFailed: false });
        case actionTypes.ADD_BOOK_SUCCESS: return addBookSuccess(state, action);
        case actionTypes.ADD_BOOK_FAIL: return updateObject(state, { error: action.error, addBookFailed: true });
        case actionTypes.ADD_RECIPE_TO_BOOK_SUCCESS: return addBookSuccess(state, action);
        case actionTypes.ADD_RECIPE_TO_BOOK_FAIL: return updateObject(state, { error: action.error, addRecipeToBookFailed: true })
        case actionTypes.LOGOUT: return updateObject(state, { user: null, isLoggedIn: null });
        default: return state;
    }
}

export default reducer;