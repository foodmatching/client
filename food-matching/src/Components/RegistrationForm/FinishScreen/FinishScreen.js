import React, { Component } from 'react';
import { Header, Content, Footer, Container, Button } from 'rsuite';
import Image from '../../UI/Image/Image';
import checkMark from '../../../assets/check-mark.png';
import crossMark from '../../../assets/cross-mark.png';
import styles from '../RegistrationForm.module.css';
import Auxiliary from '../../../Utils/HOC/Auxiliary/Auxiliary';

export default class FinishScreen extends Component {
    render() {
        let header = (
            <h1 className={styles.RegistrationSuccessScreenContainerHeader}>Congratulations!</h1>
        );

        let content = (
            <Auxiliary>
                <Image imgURL={checkMark} cssClass="RegistrationScreen" />
                <div className={styles.RegistrationScreenContainerMessage}>
                    <h3>Registration has been successfully completed!</h3>
                    <p>You may now proceed to Login to enjoy all the features we have to offer.</p>
                </div>
            </Auxiliary>
        );

        let footer = (
            <Button appearance="primary" onClick={this.props.cancelled}>Close</Button>
        );

        if (!this.props.success) {
            header = (
                <h1 className={styles.RegistrationFailScreenContainerHeader}>Aww....</h1>
            );

            content = (
                <Auxiliary>
                    <Image imgURL={crossMark} cssClass="RegistrationScreen" />
                    <div className={styles.RegistrationScreenContainerMessage}>
                        <h3>There has been an error...</h3>
                        <p>The server is busy at the moment, please try again.</p>
                    </div>
                </Auxiliary>
            );

            footer = (
                <Auxiliary>
                    <Button appearance="primary" onClick={this.props.retry}>Retry</Button>
                    <Button appearance="default" onClick={this.props.cancelled}>Cancel</Button>
                </Auxiliary>
            );
        }

        return (
            <Container>
                <Header>{header}</Header>
                <Content className={styles.RegistrationScreenContainer}>
                    {content}
                </Content>
                <Footer>
                    {footer}
                </Footer>
            </Container >
        )
    }
}
