import React, { Component } from 'react';
import FirstScreen from './FirstScreen/FirstScreen';
import styles from './RegistrationForm.module.css';
import { updateObject, fixDataStructure } from '../../Utils/utils';
import SecondScreen from './SecondScreen/SecondScreen';
import ThirdScreen from './ThirdScreen/ThirdScreen';
import FourthScreen from './FourthScreen/FourthScreen';
import FinishScreen from './FinishScreen/FinishScreen';
import axios from 'axios';
import { server } from '../../Utils/utils';

class RegistrationForm extends Component {
    state = {
        currentScreen: 1,
        maxScreens: 5,
        isRegistrationSuccessful: false,
        registrationData: {
            username: '',
            password: '',
            email: '',
            profile: {
                name: '',
                gender: '',
                height: '',
                weight: '',
                whatsLookingFor: '',
                dateOfBirth: '',
                healthIssues: [],
                ingredientsIDsLikes: [],
                typesLikes: []
            }
        }
    }

    completeRegistration = async (data) => {
        const userData = { ...data };
        //add user
        await axios.post(server.userRequests.addNewUser, { user: userData })
            .then(() => {
                this.setState({ isRegistrationSuccessful: true });
            })
            .catch(() => {
                this.setState({ isRegistrationSuccessful: false });
            });
        this.state.isRegistrationSuccessful ? this.props.lastStepStatus('finish') : this.props.lastStepStatus('error');
    }

    nextScreenHandler = () => {
        if (this.state.currentScreen < this.state.maxScreens) {
            if (this.state.currentScreen === 3) {
                this.props.lastStepStatus('process');
            }
            this.setState(prevState => {
                this.props.changeStep(prevState.currentScreen + 1);
                return { currentScreen: prevState.currentScreen + 1 }
            })
        }
    }

    prevScreenHandler = () => {
        if (this.state.currentScreen > 1) {
            if (this.state.currentScreen === 4) {
                this.props.lastStepStatus('wait');
            }
            this.setState(prevState => {
                this.props.changeStep(prevState.currentScreen - 1);
                return { currentScreen: prevState.currentScreen - 1 }
            })
        }
    }

    submitHandler = (data) => {
        const fixedData = fixDataStructure(data);
        const updatedProfileData = updateObject(this.state.registrationData.profile, fixedData.innerData);
        let updatedData = updateObject(this.state.registrationData, fixedData.outerData);
        updatedData = updateObject(updatedData, { profile: updatedProfileData });
        this.setState({ registrationData: updatedData });
        if (this.state.currentScreen === 4) {
            this.completeRegistration(updatedData);
        }
        this.nextScreenHandler()
    }

    retryHandler = () => {
        this.completeRegistration(this.state.registrationData);
    }

    render() {
        let screen = null;
        switch (this.state.currentScreen) {
            case 1:
                screen = (
                    <FirstScreen
                        switch={this.props.switch}
                        next={this.nextScreenHandler}
                        submit={this.submitHandler}
                        cancelled={this.props.cancelled} />
                );
                break;
            case 2:
                screen = (
                    <SecondScreen
                        name={this.state.registrationData.profile.name}
                        next={this.nextScreenHandler}
                        prev={this.prevScreenHandler}
                        submit={this.submitHandler}
                        cancelled={this.props.cancelled} />
                );
                break;
            case 3:
                screen = (
                    <ThirdScreen
                        next={this.nextScreenHandler}
                        prev={this.prevScreenHandler}
                        submit={this.submitHandler}
                        cancelled={this.props.cancelled} />
                );
                break;
            case 4:
                screen = (
                    <FourthScreen
                        usersHealthIssues={this.state.registrationData.profile.healthIssues}
                        prev={this.prevScreenHandler}
                        submit={this.submitHandler}
                        cancelled={this.props.cancelled} />
                );
                break;
            case 5:
                screen = (
                    <FinishScreen
                        retry={this.retryHandler}
                        cancelled={this.props.cancelled}
                        success={this.state.isRegistrationSuccessful} />
                );
                break;
            default: screen = null;
        }
        return (
            <div className={styles.FormContainer}>
                {screen}
            </div>
        )
    }
}


export default RegistrationForm;