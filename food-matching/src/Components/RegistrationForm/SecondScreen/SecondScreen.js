import React, { Component } from 'react';
import { Form, FormGroup, FormControl, Button, ButtonToolbar, ControlLabel, RadioGroup, Radio } from 'rsuite';

class SecondScreen extends Component {
    state = {
        formValue: {
            'profile.gender': 'female'
        }
    }

    checkValidationHandler = () => {
        const { formValue } = this.state;
        this.props.submit(formValue);
    }

    render() {
        const { formValue } = this.state;
        return (
            <Form
                ref={ref => (this.form = ref)}
                formValue={formValue}
                onChange={formValue => this.setState({ formValue })}>
                <FormGroup>
                    <ControlLabel>
                        <h1>Hello <b style={{ color: '#3a9bd6' }}>{this.props.name}</b>, how do you identify yourself?</h1>
                    </ControlLabel>
                    <FormControl name="profile.gender" accepter={RadioGroup}>
                        <Radio value="female">Female</Radio>
                        <Radio checked value="male">Male</Radio>
                        <Radio value="other">Other</Radio>
                    </FormControl>
                </FormGroup>
                <FormGroup>
                    <ButtonToolbar>
                        <Button appearance="primary" onClick={() => this.checkValidationHandler()}>Next Step</Button>
                        <Button appearance="default" onClick={this.props.prev}>Previous Step</Button>
                        <Button appearance="default" onClick={this.props.cancelled}>Cancel</Button>
                    </ButtonToolbar>
                </FormGroup>
            </Form>
        )
    }
}

export default SecondScreen;
