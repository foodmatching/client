import React, { Component } from 'react';
import { Panel, PanelGroup, Header, Content, Footer, Container, Button } from 'rsuite';
import styles from '../RegistrationForm.module.css';
import IngredientSelector from '../../sharedComponents/IngredientSelector/IngredientSelector';
import FoodTypeSelector from '../../sharedComponents/FoodTypeSelector/FoodTypeSelector';

class FourthScreen extends Component {
    state = {
        'profile.ingredientsIDsLikes': [],
        'profile.typesLikes': [],
        allIngredientsList: null
    }

    finishHandler = () => {
        this.props.submit(this.state);
    }

    foodTypeClicked = (value) => {
        const foodTypesArr = [...this.state["profile.typesLikes"]];
        const indexOfItem = foodTypesArr.indexOf(value);
        if (indexOfItem >= 0) {
            foodTypesArr.splice(indexOfItem, 1);
        } else {
            foodTypesArr.push(value);
        }
        this.setState({ 'profile.typesLikes': foodTypesArr });
    }

    ingredientClicked = (id) => {
        const ingredientsArr = [...this.state["profile.ingredientsIDsLikes"]];
        const indexOfItem = ingredientsArr.indexOf(id);
        if (indexOfItem >= 0) {
            ingredientsArr.splice(indexOfItem, 1);
        } else {
            ingredientsArr.push(id);
        }
        this.setState({ 'profile.ingredientsIDsLikes': ingredientsArr });
    }

    render() {
        return (
            <Container>
                <Header>Pick food types and ingredients you like to help us match better food for you.</Header>
                <Content>
                    <PanelGroup accordion bordered defaultActiveKey={1} className={styles.PanelGroupContainer}>
                        <Panel header="Food Types" bodyFill eventKey={1}>
                            <div className={styles.PanelContainer}>
                                <FoodTypeSelector
                                    selectedFoodTypes={this.state["profile.typesLikes"]}
                                    foodTypeClicked={this.foodTypeClicked} />
                            </div>
                        </Panel>
                        <Panel header="Ingredients" bodyFill eventKey={2}>
                            <div className={styles.PanelContainer}>
                                <IngredientSelector
                                    usersHealthIssues={this.props.usersHealthIssues}
                                    selectedIngredients={this.state["profile.ingredientsIDsLikes"]}
                                    ingredientClicked={this.ingredientClicked} />
                            </div>
                        </Panel>
                    </PanelGroup>
                </Content>
                <Footer className={styles.FooterContainer}>
                    <Button appearance="primary" onClick={() => this.finishHandler()}>Finish</Button>
                    <Button appearance="default" onClick={this.props.prev}>Previous Step</Button>
                    <Button appearance="default" onClick={this.props.cancelled}>Cancel</Button>
                </Footer>
            </Container>

        )
    }
}

export default FourthScreen;