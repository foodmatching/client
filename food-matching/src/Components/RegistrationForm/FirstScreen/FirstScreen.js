import React, { Component } from 'react';
import { Form, FormGroup, FormControl, HelpBlock, Button, ButtonToolbar } from 'rsuite';
import styles from '../RegistrationForm.module.css';
import { firstScreenModel } from '../../../Utils/formValidations';
import axios from 'axios';
import { server } from '../../../Utils/utils';

class FirstScreen extends Component {
    state = {
        formValue: {
            username: '',
            isUsernameAvailable: null,
            password: '',
            verifyPassword: '',
            email: '',
            'profile.name': ''
        },
        formError: {},
    }

    checkValidationHandler = async () => {
        const { formValue } = this.state;
        let available = false;
        if (formValue && formValue.username !== '') {
            await axios.post(server.userRequests.getUserByUserName, { username: formValue.username })
                .then(() => {
                    available = false;
                })
                .catch(() => {
                    available = true;
                });
            formValue.isUsernameAvailable = available;
        }
        if (!this.form.check()) {
            return;
        }
        this.props.submit(formValue);
    }

    render() {
        const { formValue } = this.state;
        return (
            <Form
                ref={ref => (this.form = ref)}
                formValue={formValue}
                model={firstScreenModel}
                onChange={formValue => this.setState({ formValue })}
                onCheck={formError => this.setState({ formError })}
                checkTrigger="none">
                <FormGroup>
                    <FormControl name="username" placeholder="Username (4-8 characters)" />
                    <HelpBlock className={styles.FormContainerHelpBlock}>* Required</HelpBlock>
                </FormGroup>
                <FormGroup>
                    <FormControl name="password" type="password" placeholder="Password (8-16 characters)" />
                    <HelpBlock className={styles.FormContainerHelpBlock}>* Required</HelpBlock>
                </FormGroup>
                <FormGroup>
                    <FormControl name="verifyPassword" type="password" placeholder="Verify Password" />
                    <HelpBlock className={styles.FormContainerHelpBlock}>* Required</HelpBlock>
                </FormGroup>
                <FormGroup>
                    <FormControl name="email" type="email" placeholder="Email Address" />
                    <HelpBlock className={styles.FormContainerHelpBlock}>* Required</HelpBlock>
                </FormGroup>
                <FormGroup>
                    <FormControl name="profile.name" placeholder="Your Name" checkTrigger="change" />
                    <HelpBlock className={styles.FormContainerHelpBlock}>* Required</HelpBlock>
                </FormGroup>
                <FormGroup>
                    <ButtonToolbar>
                        <Button appearance="link" onClick={this.props.switch}>Already have an account yet? Click here!</Button>
                        <br />
                        <Button appearance="primary" onClick={() => this.checkValidationHandler()}>Next Step</Button>
                        <Button appearance="default" onClick={this.props.cancelled}>Cancel</Button>
                    </ButtonToolbar>
                </FormGroup>
            </Form>
        )
    }
}

export default FirstScreen;
