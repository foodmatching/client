import { Form, FormGroup, FormControl, HelpBlock, Button, ButtonToolbar, CheckPicker, InputNumber, DatePicker, ControlLabel } from 'rsuite';
import styles from '../RegistrationForm.module.css';
import { healthIssuesDataList, thirdScreenModel } from '../../../Utils/formValidations';
import React, { Component } from 'react'

class ThirdScreen extends Component {
    state = {
        formValue: {
            'profile.height': '',
            'profile.weight': '',
            'profile.whatsLookingFor': '',
            'profile.dateOfBirth': new Date('2000-01-01'),
            'profile.healthIssues': []
        },
        formError: {},
    }

    checkValidationHandler = async () => {
        const { formValue } = this.state;
        if (!this.form.check()) {
            return;
        }

        this.props.submit(formValue);
    }

    render() {
        const { formValue } = this.state;
        let healthIssuesList = formValue["profile.healthIssues"].length <= 0 ? null : formValue["profile.healthIssues"].map((issue, index) => {
            return (
                <li key={index}>{issue}</li>
            )
        })
        return (
            <Form
                ref={ref => (this.form = ref)}
                formValue={formValue}
                model={thirdScreenModel}
                onChange={formValue => this.setState({ formValue })}
                onCheck={formError => this.setState({ formError })}
                checkTrigger="none">
                <div style={{ paddingBottom: '24px' }}>
                    <ControlLabel><h3><b>Help us get to know you a bit ...</b></h3></ControlLabel>
                </div>
                <FormGroup className={styles.FormGroupInline}>
                    <FormControl style={{ width: 120 }} name="profile.height" placeholder="height (cm)" accepter={InputNumber} />
                    <HelpBlock tooltip className={styles.FormContainerHelpBlock}>Required</HelpBlock>
                    <div style={{ width: '10px' }} />
                    <FormControl style={{ width: 120 }} name="profile.weight" placeholder="weight (kg)" accepter={InputNumber} />
                    <HelpBlock tooltip className={styles.FormContainerHelpBlock}>Required</HelpBlock>
                </FormGroup>
                <FormGroup>
                    <FormControl name="profile.whatsLookingFor" placeholder="What are you looking for?" />
                </FormGroup>
                <FormGroup>
                    <FormControl name="profile.dateOfBirth"
                        accepter={DatePicker}
                        style={{ width: 300 }}
                        format={'MM-DD-YYYY'}
                        oneTap
                        ranges={[]} />
                </FormGroup>
                <FormGroup>
                    <FormControl name="profile.healthIssues" accepter={CheckPicker} data={healthIssuesDataList} style={{ width: 300 }} />
                    <div
                        className={styles.HealthIssuesDisplayListContainer}
                        style={formValue["profile.healthIssues"].length <= 0 ? { visibility: 'hidden' } : { visibility: 'visible' }}>
                        <ul>
                            {healthIssuesList}
                        </ul>
                    </div>
                </FormGroup>
                <FormGroup>
                    <ButtonToolbar>
                        <Button appearance="primary" onClick={() => this.checkValidationHandler()}>Next Step</Button>
                        <Button appearance="default" onClick={this.props.prev}>Previous Step</Button>
                        <Button appearance="default" onClick={this.props.cancelled}>Cancel</Button>
                    </ButtonToolbar>
                </FormGroup>
            </Form>
        )
    }
}

export default ThirdScreen;