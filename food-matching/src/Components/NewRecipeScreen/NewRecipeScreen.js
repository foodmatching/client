import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal } from "rsuite";
import * as actions from '../../store/actions';
import NewRecipe from './NewRecipe/NewRecipe';
import styles from './NewRecipe/NewRecipe.module.css';
import NewRecipeFinishScreen from './NewRecipeFinishScreen/NewRecipeFinishScreen';

class NewRecipeScreen extends Component {
    state = {
        newRecipeSubmitted: false
    }

    submitRecipe = (recipe, picture, userID) => {
        this.setState({ newRecipeSubmitted: true });
        this.props.onAddNewRecipe(recipe, picture, userID);
    }

    render() {
        let content = (
            <NewRecipe
                userID={this.props.user._id}
                submit={this.submitRecipe}
                cancel={this.props.onHideNewRecipeModal} />
        );

        if (this.state.newRecipeSubmitted) {
            content = (
                <NewRecipeFinishScreen
                    success={this.props.newRecipeUploadSuccess}
                    cancel={this.props.onHideNewRecipeModal} />
            );
        }

        return (
            <Modal
                size='md'
                backdrop='static'
                show={this.props.showNewRecipe}
                onHide={() => this.props.onHideNewRecipeModal()}>
                <Modal.Header>
                    <Modal.Title>
                        <div className={styles.Header}>Write your own recipe</div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {content}
                </Modal.Body>
            </Modal>
        )
    }
}

const mapStateToProps = state => {
    return {
        showNewRecipe: state.recipes.showNewRecipeModal,
        user: state.users.user,
        loading: state.recipes.loading,
        newRecipeUploadSuccess: state.recipes.newRecipeUploadSuccess
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onHideNewRecipeModal: () => dispatch(actions.hideNewRecipeModal()),
        onAddNewRecipe: (recipe, picture, username) => dispatch(actions.addNewRecipe(recipe, picture, username))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewRecipeScreen);