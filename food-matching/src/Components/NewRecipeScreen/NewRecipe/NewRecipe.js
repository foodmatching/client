import React, { Component } from 'react';
import { Form, FormGroup, ControlLabel, FormControl, HelpBlock, Button, ButtonToolbar, SelectPicker, Icon, InputNumber, Tag, TagGroup } from 'rsuite';
import { Icon as SemanticIcon } from 'semantic-ui-react'
import ImageUploader from 'react-images-upload';
import { unitData } from '../../../Utils/formValidations';
import styles from './NewRecipe.module.css';
import axios from 'axios';
import { server, deepCloneObject } from '../../../Utils/utils';

class NewRecipe extends Component {
    state = {
        formValue: {
            title: '',
            ingredient: '',
            ingredientAmount: 0,
            ingredientMeasurementUnit: null,
            instructions: ''
        },
        formError: {},
        ingredients: [],
        items: [],
        picture: null
    }

    checkValidationHandler = () => {
        const reformattedIngredients = this.state.ingredients.map(el => {
            return {
                ingredientId: el.id,
                amount: el.amount,
                unit: el.unit
            }
        })
        const recipe = {
            name: this.state.formValue.title,
            ingredients: reformattedIngredients,
            instructions: this.state.formValue.instructions
        }
        this.props.submit(recipe, this.state.picture, this.props.userID);
    }

    ingredientsOpenHandler = () => {
        if (this.state.items.length === 0) {
            axios.post(server.ingredientRequests.getAllIngredients, { size: 50 })
                .then(response => {
                    const items = [];
                    response.data.forEach(el => {
                        const item = {
                            label: el.name,
                            value: el._id,
                            role: 'Master'
                        }
                        items.push(item);
                    })
                    this.setState({ items: items });
                })
                .catch();
        }
    }

    ingredientsSearchHandler = value => {
        axios.post(server.ingredientRequests.GetIngredientStartWith, { name: value })
            .then(response => {
                const items = [];
                response.data.forEach(el => {
                    const item = {
                        label: el.name,
                        value: el._id,
                        role: 'Master'
                    }
                    items.push(item);
                })
                this.setState({ items: items });
            })
            .catch();
    }

    addIngredientHandler = () => {
        const updatedFormValue = {
            ...this.state.formValue,
            ingredient: '',
            ingredientAmount: 0,
            ingredientMeasurementUnit: null
        }
        const ingredients = deepCloneObject(this.state.ingredients);
        const value = this.state.formValue.ingredient;
        const ingredient = {
            name: '',
            id: value,
            amount: this.state.formValue.ingredientAmount,
            unit: this.state.formValue.ingredientMeasurementUnit
        };
        const item = this.state.items.find(el => el.value === value);
        ingredient.name = item.label;
        ingredients.push(ingredient);
        this.setState({ ingredients: ingredients, formValue: updatedFormValue });
    }

    removeIngredientHandler = (value) => {
        const updatedIngredients = this.state.ingredients.filter(el => el.name !== value);
        this.setState({ ingredients: updatedIngredients })
    }

    uploadHandler = value => {
        this.setState({ picture: value });
    }

    render() {
        const { formValue, items } = this.state;

        const addButtonDisabled = formValue.ingredient !== '' && formValue.ingredientAmount > 0 && formValue.ingredientMeasurementUnit !== null ? false : true;
        const submitButtonDisabled = formValue.title !== '' && formValue.instructions !== '' && this.state.ingredients.length > 0 ? false : true;

        let disabledOptions = [];
        if (this.state.ingredients.length > 0) {
            disabledOptions = this.state.ingredients.map(el => el.id)
        }

        let ingredientsPreview = null;
        if (this.state.ingredients.length > 0) {
            ingredientsPreview = this.state.ingredients.map(ingredient => {
                return (
                    <Tag
                        key={ingredient.id}
                        onClose={() => this.removeIngredientHandler(ingredient.name)}
                        closable
                        color='cyan'>{ingredient.name}</Tag>
                );
            })
        }

        let imageUploader = (
            <ImageUploader
                withIcon={true}
                buttonText='Choose a picture'
                onChange={this.uploadHandler}
                imgExtension={['.jpg', '.gif', '.png', '.gif']}
                maxFileSize={5242880}
                singleImage={true}
            />
        );
        if (this.state.picture) {
            imageUploader = (
                <SemanticIcon.Group size='huge'>
                    <SemanticIcon name='picture' color='teal' />
                    <SemanticIcon corner='bottom right' color='green' name='check circle' />
                </SemanticIcon.Group>
            )
        }

        return (
            <div className={styles.FormContainer}>
                <Form
                    ref={ref => (this.form = ref)}
                    formValue={formValue}
                    onChange={formValue => this.setState({ formValue })}
                    onCheck={formError => this.setState({ formError })}
                    checkTrigger="none">
                    <FormGroup>
                        <FormControl name="title" placeholder="title" />
                        <HelpBlock tooltip>The title for your recipe</HelpBlock>
                    </FormGroup>
                    {/* --Ingredients-- */}
                    <FormGroup>
                        <ControlLabel>Pick Ingredients, Quantity and Measurement units</ControlLabel>
                        <ButtonToolbar>
                            <FormControl
                                labelKey='label'
                                placeholder="Select Ingredient"
                                accepter={SelectPicker}
                                name="ingredient"
                                data={items}
                                disabledItemValues={disabledOptions}
                                style={{ width: 'fit-content' }}
                                onOpen={this.ingredientsOpenHandler}
                                onSearch={this.ingredientsSearchHandler}
                                renderMenu={menu => {
                                    if (items.length === 0) {
                                        return (
                                            <p style={{ padding: 4, color: '#3a9bd6', textAlign: 'center' }}>
                                                <Icon icon="spinner" spin /> Loading...
                                        </p>
                                        );
                                    }
                                    return menu;
                                }} />
                            <FormControl
                                style={{ width: '80px' }}
                                name="ingredientAmount"
                                min={0.001}
                                accepter={InputNumber} />
                            <FormControl
                                placeholder="Select Measurement unit"
                                accepter={SelectPicker}
                                name="ingredientMeasurementUnit"
                                data={unitData}
                                style={{ width: 'fit-content' }} />
                            <Button
                                disabled={addButtonDisabled}
                                appearance="primary"
                                onClick={() => this.addIngredientHandler()}>Add</Button>
                        </ButtonToolbar>
                        <TagGroup style={{ padding: '10px' }}>
                            {ingredientsPreview}
                        </TagGroup>
                    </FormGroup>
                    {/* instructions */}
                    <FormGroup>
                        <ControlLabel>Instructions</ControlLabel>
                        <FormControl
                            componentClass="textarea"
                            rows={6}
                            style={{ width: 700 }}
                            name="instructions"
                            placeholder="Tell us how to make the dish" />
                    </FormGroup>

                    <FormGroup>
                        {imageUploader}
                    </FormGroup>
                    <FormGroup>
                        <ButtonToolbar>
                            <Button
                                disabled={submitButtonDisabled}
                                appearance="primary"
                                onClick={() => this.checkValidationHandler()}>Create</Button>
                            <Button appearance="default" onClick={() => this.props.cancel()}>Cancel</Button>
                        </ButtonToolbar>
                    </FormGroup>
                </Form>
            </div>
        )
    }
}

export default NewRecipe;
