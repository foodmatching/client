import React, { Component } from 'react';
import { Header, Content, Footer, Container, Button } from 'rsuite';
import Image from '../../UI/Image/Image';
import checkMark from '../../../assets/check-mark.png';
import crossMark from '../../../assets/cross-mark.png';
import styles from './NewRecipeFinishScreen.module.css';
import Auxiliary from '../../../Utils/HOC/Auxiliary/Auxiliary';
import { connect } from 'react-redux';
import LoaderComponent from '../../sharedComponents/LoaderComponent/LoaderComponent';

class FinishScreen extends Component {
    render() {
        let header = (
            <h1 className={styles.RegistrationSuccessScreenContainerHeader}>Thank you for your contribution!</h1>
        );

        let content = (
            <Auxiliary>
                <Image imgURL={checkMark} cssClass="RegistrationScreen" />
                <div className={styles.RegistrationScreenContainerMessage}>
                    <h3>Your recipe has been successfully added to our website!</h3>
                </div>
            </Auxiliary>
        );

        let footer = (
            <Button appearance="primary" onClick={this.props.cancel}>Close</Button>
        );

        if (!this.props.success) {
            header = (
                <h1 className={styles.RegistrationFailScreenContainerHeader}>Aww....</h1>
            );

            content = (
                <Auxiliary>
                    <Image imgURL={crossMark} cssClass="RegistrationScreen" />
                    <div className={styles.RegistrationScreenContainerMessage}>
                        <h3>There has been an error...</h3>
                        <p>The server is busy at the moment, please try again.</p>
                    </div>
                </Auxiliary>
            );

            footer = (
                <Auxiliary>
                    <Button appearance="default" onClick={this.props.cancel}>Close</Button>
                </Auxiliary>
            );
        }

        const loader = (
            <Container>
                <div style={{ height: '313.6px' }}>
                    <LoaderComponent />;
                </div>
            </Container>
        )
        const container = (
            <Container>
                <Header>{header}</Header>
                <Content className={styles.RegistrationScreenContainer}>
                    {content}
                </Content>
                <Footer>
                    {footer}
                </Footer>
            </Container >
        )

        return this.props.loading ? loader : container
    }
}

const mapStateToProps = state => {
    return {
        loading: state.recipes.addNewRecipeLoading
    }
}

export default connect(mapStateToProps)(FinishScreen); 