import React from 'react';
import foodiesLogo from '../../../assets/logo.png';
import styles from './Logo.module.css'

const Logo = () => (
    <div className={styles.Logo}>
        <img src={foodiesLogo} alt="Burger" />
    </div>
);

export default Logo;
