import React, { Component } from "react";
import { NavLink as Link, withRouter } from 'react-router-dom';
import { Collapse, Navbar, NavbarToggler, Nav, NavItem, NavLink, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import MenuIcon from "./MenuIcon/MenuIcon";
import Logo from "./Logo/Logo";
import Auxiliary from '../../Utils/HOC/Auxiliary/Auxiliary';
import { connect } from 'react-redux'
import * as actions from '../../store/actions';
import styles from './NavBar.module.css';
import { AutoComplete, InputGroup, Icon } from 'rsuite';
import axios from 'axios';
import { server } from '../../Utils/utils';

class NavBar extends Component {
  state = {
    isOpen: false,
    items: [],
    value: '',
    data: null
  }

  toggle = () => {
    this.setState(prevState => ({ isOpen: !prevState.isOpen }));
  }

  logoutHandler = () => {
    this.props.onLogout();
    this.props.history.push('/');
  }

  handleChange = async (value) => {
    await axios.post(server.recipeRequests.GetRecipesContainingNamesAndIds, { name: value })
      .then(response => {
        let items = [];
        response.data.forEach(item => {
          items.push(item.name)
        })
        this.setState({ data: response.data, items: items, value: value });
      })
      .catch(console.log);
  }

  handleSelect = (value) => {
    const recipeID = this.state.data.find(el => el.name === value.value).id;
    this.props.history.push(`/recipe/${recipeID}`)
  }

  handleSearch = (value) => {

  }

  render() {
    const navItemStyle = { padding: '8px' };
    const linkStyle = { color: 'rgba(0,0,0,0.5)', textDecoration: 'none' };

    let userNavItems = (
      <Auxiliary>
        <NavItem>
          <NavLink className={styles.NavLinkHover}>
            <button className={styles.NavBarButton} onClick={() => this.props.onShowRegistrationModal()}>Register</button>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink className={styles.NavLinkHover}>
            <button className={styles.NavBarButton} onClick={() => this.props.onShowLoginModal()}>Login</button>
          </NavLink>
        </NavItem>
      </Auxiliary>
    );

    let dropdownMenu = null;

    if (this.props.isLoggedIn) {
      userNavItems = (
        <Auxiliary>
          <NavItem className={styles.NavLinkHover} style={navItemStyle}>
            <Link activeStyle={linkStyle} className={styles.NavLink} to="/user/cookbooks" exact>{this.props.user.profile.name}</Link>
          </NavItem>
        </Auxiliary>
      );

      dropdownMenu = (
        <UncontrolledDropdown nav inNavbar>
          <DropdownToggle nav tag="span">
            <MenuIcon />
          </DropdownToggle>
          <DropdownMenu right>
            <NavLink className={styles.NoPadding}>
              <DropdownItem onClick={() => this.props.onShowNewRecipeModal()}>Write your own recipe</DropdownItem>
            </NavLink>
            <NavLink className={styles.NoPadding}>
              <DropdownItem>Settings</DropdownItem>
            </NavLink>
            <DropdownItem divider />
            <NavLink className={styles.NoPadding}>
              <DropdownItem onClick={() => this.logoutHandler()}>Logout</DropdownItem>
            </NavLink>
          </DropdownMenu>
        </UncontrolledDropdown>
      );
    }

    const { items, value } = this.state;

    return (
      <div>
        <Navbar light expand="md">
          <Link className={styles.MarginRight} to="/" exact><Logo /></Link>
          <InputGroup style={{ width: '100%' }}>
            <AutoComplete
              selectOnEnter={true}
              data={items}
              value={value}
              onSelect={this.handleSelect}
              onChange={this.handleChange} />
            <InputGroup.Button onClick={this.handleSearch}>
              <Icon icon="search" />
            </InputGroup.Button>
          </InputGroup>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar style={{ maxInlineSize: 'fit-content', flexBasis: '100%' }}>
            <Nav className="ml-auto" navbar>
              <NavItem className={styles.NavLinkHover} style={navItemStyle}>
                <Link activeStyle={linkStyle} className={styles.NavLink} to="/" exact>Home</Link>
              </NavItem>
              {userNavItems}
              {dropdownMenu}
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    showRegistration: state.users.showRegistrationModal,
    showLogin: state.users.showLogin,
    showNewRecipe: state.recipes.showNewRecipeModal,
    recipes: state.recipes.recipeList
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onShowRegistrationModal: () => dispatch(actions.showRegistrationModal()),
    onShowLoginModal: () => dispatch(actions.showLoginModal()),
    onShowNewRecipeModal: () => dispatch(actions.showNewRecipeModal()),
    onLogout: () => dispatch(actions.logout())
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(NavBar));