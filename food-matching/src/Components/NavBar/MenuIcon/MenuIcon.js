import React from 'react';

import menuIcon from '../../../assets/menu-icon.png';
import styles from './MenuIcon.module.css'

const MenuIcon = () => (
    <div className={styles.Menu}>
        <img src={menuIcon} alt="menu" />
    </div>
);

export default MenuIcon;
