import React from 'react'
import './Image.css'
import { arrayBufferToBase64 } from '../../../Utils/utils';
import noImageAvailable from '../../../assets/no-image-available-icon-6.jpg';

const Image = (props) => {
    let innerCssClass = "RecipeScreenInnerImage";
    if (props.innerCssClass) {
        innerCssClass = props.innerCssClass
    }
    let img = (
        <div className={props.cssClass}>
            <div className={innerCssClass} style={{ content: `url(${props.imgURL ? props.imgURL : noImageAvailable})` }} />
        </div>
    )

    if (props.picture && props.picture.contentType.length > 6) {
        const base64Flag = 'data:image/jpeg;base64,';
        const imageStr = arrayBufferToBase64(props.picture.data.data);
        img = (
            <div className={props.cssClass}>
                <div className={innerCssClass} style={{ content: `url(${base64Flag + imageStr})` }} />
            </div>
        )
    }
    return img;
}

export default Image
