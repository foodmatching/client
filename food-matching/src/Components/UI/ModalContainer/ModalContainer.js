import React, { Component } from 'react';
import { Modal, Steps } from "rsuite";
import { connect } from 'react-redux'
import * as actions from '../../../store/actions';
import RegistrationForm from '../../RegistrationForm/RegistrationForm';
import LoginScreen from '../../LoginScreen/LoginScreen';
import styles from './ModalContainer.module.css';

class ModalContainer extends Component {
    state = {
        size: 'sm',
        step: 1,
        lastStepStatus: 'wait' //error,wait,process,finish
    }

    switchBetweenLoginAndRegistrationHandler = () => {
        if (this.props.showLogin) {
            this.props.onShowRegistrationModal();
        } else {
            this.props.onShowLoginModal();
        }
    }

    stepHandler = step => {
        switch (step) {
            case 3:
            case 4:
                this.setState({ size: 'lg' });
                break;
            default: this.setState({ size: 'sm' })
        }
        this.setState({ step: step });
    }

    lastStepStatusHandler = status => {
        this.setState({ lastStepStatus: status });
    }

    render() {
        let modalContent, modalHeader, modalTitle = null;
        if (this.props.showRegistration) {
            modalContent = (
                <RegistrationForm
                    switch={() => this.switchBetweenLoginAndRegistrationHandler()}
                    changeStep={(step) => this.stepHandler(step)}
                    lastStepStatus={(status => this.lastStepStatusHandler(status))}
                    cancelled={() => this.props.onHideRegistrationModal()} />
            );

            modalHeader = (
                <Steps small current={this.state.step - 1}>
                    <Steps.Item title="Step 1" />
                    <Steps.Item title="Step 2" />
                    <Steps.Item title="Step 3" />
                    <Steps.Item title="Step 4" status={this.state.lastStepStatus} />
                </Steps>
            );
            modalTitle = 'Sign-Up for Foodies';
        }

        if (this.props.showLogin) {
            modalContent = (
                <LoginScreen
                    switch={() => this.switchBetweenLoginAndRegistrationHandler()}
                    cancelled={() => this.props.onHideLoginModal()} />
            );

            modalHeader = null;

            modalTitle = 'Login to enjoy all our features!';
        }

        return (
            <Modal
                size={this.props.showRegistration ? this.state.size : 'xs'}
                backdrop="static"
                show={this.props.showRegistration || this.props.showLogin}
                onHide={() => this.props.showLogin ? this.props.onHideLoginModal() : this.props.onHideRegistrationModal()}>
                <Modal.Header>
                    <Modal.Title className={styles.ModalHeader}>{modalTitle}</Modal.Title>
                    {modalHeader}
                </Modal.Header>
                <Modal.Body>
                    {modalContent}
                </Modal.Body>
            </Modal>
        )
    }
}

const mapStateToProps = state => {
    return {
        showRegistration: state.users.showRegistration,
        showLogin: state.users.showLogin
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onHideRegistrationModal: () => dispatch(actions.hideRegistrationModal()),
        onHideLoginModal: () => dispatch(actions.hideLoginModal()),
        onShowLoginModal: () => dispatch(actions.showLoginModal()),
        onShowRegistrationModal: () => dispatch(actions.showRegistrationModal()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalContainer);