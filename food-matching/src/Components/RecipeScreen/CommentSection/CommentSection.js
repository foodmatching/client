import React, { Component } from 'react';
import { Button, Comment, Form, Rating } from 'semantic-ui-react';
import ImageUploader from 'react-images-upload';
import { getTimeString, getPictureSrc, server } from '../../../Utils/utils';
import personAvatar from '../../../assets/person-avatar.png';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions';
import Auxiliary from '../../../Utils/HOC/Auxiliary/Auxiliary';
import axios from 'axios'

class CommentSection extends Component {
    state = {
        newCommentText: '',
        rating: 0,
        picture: null,
        showUploader: false,
        comments: null
    }

    async componentDidMount() {
        const transformedComments = await this.transformComments(this.props.comments);
        this.setState({ comments: transformedComments });
    }

    async componentDidUpdate(prevProps, prevState) {
        if (prevProps.comments !== this.props.comments) {
            const transformedComments = await this.transformComments(this.props.comments);
            this.setState({ comments: transformedComments });
        }

    }

    transformComments = async (comments) => {
        let updatedComments = [];
        for (let comment of comments) {
            if (comment.userID) {
                await axios.post(server.userRequests.getUserNameAndPictureByID, { id: comment.userID })
                    .then(res => {
                        updatedComments.push({
                            ...comment,
                            user: {
                                name: res.data.profile.name,
                                pic: res.data.profile.picture
                            }
                        });
                    })
            } else {
                updatedComments.push({
                    ...comment,
                    user: {
                        name: 'Someone',
                        pic: null
                    }
                })
            }
        }
        return updatedComments;
    }

    addCommentHandler = () => {
        const comment = {
            userID: this.props.user._id,
            text: this.state.newCommentText,
            rank: this.state.rating
        }
        const picture = this.state.picture;
        this.props.onAddCommentToRecipe(this.props.recipeID, comment, picture);
        this.setState({ newCommentText: '', rating: 0 });
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    handleRate = (e, { rating }) => this.setState({ rating })

    uploadHandler = value => {
        this.setState({ picture: value, showUploader: false });
    }

    initUploaderHandler = () => {
        this.setState({ picture: null, showUploader: true });
    }

    render() {
        let comments = null;
        if (this.state.comments) {
            comments = this.state.comments.map(comment => {
                return (
                    <Comment key={comment._id}>
                        <Comment.Avatar as='a' src={comment.user.pic ? getPictureSrc(comment.user.pic) : personAvatar} />
                        <Comment.Content>
                            <Comment.Author as='a'>{comment.user.name}</Comment.Author>
                            <Comment.Metadata>
                                <span>{comment.time ? getTimeString(comment.time) : getTimeString(null)}</span>
                            </Comment.Metadata>
                            <Comment.Text>{comment.text}</Comment.Text>
                        </Comment.Content>
                    </Comment>
                )
            });
        }

        let commentButton = (
            <Button content='Please login to add comments' labelPosition='left' icon='edit' disabled primary />
        );

        let rating = (
            <Rating icon='heart' disabled maxRating={5} />
        );

        let pictureUploader = (
            <Auxiliary>
                <div style={{ display: !this.state.showUploader ? 'inherit' : 'none', paddingBottom: '10px' }}>
                    <Button
                        onClick={() => this.initUploaderHandler()}
                        content={this.state.picture ? 'Want to change your selection?' : 'Want to add a picture?'}
                        disabled={!this.props.user && !this.state.picture ? true : false} />
                </div>

                <div style={{ display: this.state.showUploader ? 'inherit' : 'none', paddingBottom: '10px' }}>
                    <ImageUploader
                        key={this.state.picture ? 1 : 0}
                        withIcon={true}
                        buttonText='Choose a picture'
                        onChange={this.uploadHandler}
                        imgExtension={['.jpg', '.gif', '.png', '.gif']}
                        maxFileSize={5242880}
                        singleImage={true}
                        withPreview={true}
                    />
                </div>
            </Auxiliary>
        );

        if (this.props.user) {
            commentButton = (
                <Button
                    content='Add comment'
                    labelPosition='left'
                    icon='edit'
                    primary
                    loading={this.props.btnLoading}
                    onClick={() => this.addCommentHandler()} />
            );
            rating = (
                <Rating icon='heart' rating={this.state.rating} maxRating={5} onRate={this.handleRate} />
            );
        }

        const { newCommentText } = this.state;

        return (
            <div className={this.props.className}>
                <Comment.Group minimal size='large'>
                    {comments}
                    <Form reply>
                        <Form.TextArea
                            placeholder='Enter your comment here'
                            name='newCommentText'
                            value={newCommentText}
                            onChange={this.handleChange}
                            disabled={this.props.user ? false : true} />
                        <div style={{ paddingBottom: '10px' }}>Rate this recipe:{rating}</div>
                        {pictureUploader}
                        {commentButton}
                    </Form>
                </Comment.Group>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        btnLoading: state.recipes.addCommentLoading
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAddCommentToRecipe: (recipeID, comment) => dispatch(actions.addCommentToRecipe(recipeID, comment))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CommentSection);