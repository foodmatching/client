import React from "react";
import "./RecipeHeader.css";
import { Button, Icon, Rating } from 'semantic-ui-react';
import { SelectPicker } from 'rsuite';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions';
import Auxiliary from '../../../Utils/HOC/Auxiliary/Auxiliary';

class RecipeHeader extends React.Component {
    state = {
        showCookBookSelector: false,
        selectedCookBook: '',
        isSaved: false
    }

    saveRecipeToCookBookHandler = () => {
        //u-id,cb-name,r-id
        const userID = this.props.user._id;
        const recipeID = this.props.recipeID;
        const bookName = this.state.selectedCookBook;
        this.props.onSaveRecipeToCookBook(userID, bookName, recipeID);
    }

    selectorHandler = value => {
        this.setState({ selectedCookBook: value });
    }

    savedRecipeHandler = cookBook => {
        const recipeIDs = cookBook.recipes;
        for (let i = 0; i < recipeIDs.length; i++) {
            if (recipeIDs[i].recipeId === this.props.recipeID) {
                this.setState({ isSaved: true });
                return;
            }
        }
    }

    goBackHandler = () => {
        this.props.history.goBack();
    }

    saveCookBookSelectorToggleHandler = () => {
        this.setState(prevState => { return { showCookBookSelector: !prevState.showCookBookSelector } })
    }

    render() {
        const rating = Math.round(this.props.rating);

        let saveCookBookSelector = null;
        if (this.props.user) {
            const cookBookList = this.props.user.cookBooks.map(book => {
                if (!this.state.isSaved) {
                    this.savedRecipeHandler(book)
                }
                return {
                    label: book.name,
                    value: book.name,
                    role: 'Master'
                }
            });

            const canSave = this.state.selectedCookBook === '' ? false : true;
            saveCookBookSelector = (
                <Auxiliary>
                    <SelectPicker
                        onChange={this.selectorHandler}
                        value={this.state.selectedCookBook}
                        style={{ width: 224, paddingRight: '8px' }}
                        data={cookBookList} />
                    <Button
                        inverted color='blue'
                        disabled={!canSave}
                        onClick={this.saveRecipeToCookBookHandler}
                        size='small' animated>
                        <Button.Content visible>Add</Button.Content>
                        <Button.Content hidden>
                            <Icon name='save' />
                        </Button.Content>
                    </Button>
                </Auxiliary>
            );
        }

        return (
            <div className="HeaderContainer">
                <div className="BackButton">
                    <Button
                        onClick={this.goBackHandler}
                        size='large' animated>
                        <Button.Content visible>Back</Button.Content>
                        <Button.Content hidden>
                            <Icon name='arrow left' />
                        </Button.Content>
                    </Button>
                </div>
                <div className="HeaderSubContainer">
                    <div>
                        <h1>{this.props.title}</h1>
                    </div>
                    <div>
                        <Rating
                            icon='heart'
                            disabled
                            size='massive'
                            rating={rating}
                            maxRating={5} />
                    </div>
                    <div>
                        <Button
                            color={this.state.isSaved ? 'green' : this.props.saveFailed ? 'red' : null}
                            disabled={this.props.user && !this.state.isSaved ? false : true}
                            onClick={this.saveCookBookSelectorToggleHandler}
                            size='medium' animated>
                            <Button.Content visible>
                                {this.state.isSaved ? 'Saved' : this.props.saveFailed ? 'Feature Unavailable' : 'Save Recipe'}
                            </Button.Content>
                            <Button.Content hidden>
                                <Icon name='add square' />
                            </Button.Content>
                        </Button>
                    </div>
                    <div>
                        {this.state.showCookBookSelector && !this.state.isSaved ? saveCookBookSelector : null}
                    </div>
                </div>
            </div>)
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        saveFailed: state.users.addRecipeToBookFailed
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSaveRecipeToCookBook: (userID, bookName, recipeID) => dispatch(actions.addRecipeToCookBook(userID, bookName, recipeID))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(RecipeHeader));