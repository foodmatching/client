import React, { Component } from 'react';
import styles from './NutritionValues.module.css';

export default class NutritionalValues extends Component {
    render() {
        const nutValues = Object.keys(this.props.values).map(key => {
            return [key, this.props.values[key]]
        });
        const headers = nutValues.map((val, index) => (
            <li key={index}>{val[0]}</li>
        ))
        const values = nutValues.map((val, index) => (
            <li key={index}>{val[1].toFixed(2)}</li>
        ))

        return (
            <div className={styles.NutritionValues}>
                <ul className={styles.Headers}>
                    {headers}
                </ul>
                <ul>
                    {values}
                </ul>
            </div>
        )
    }
}
