import React from "react";
import RecipeHeader from "./RecipeHeader/RecipeHeader";
import styles from "./RecipeScreen.module.css";
import CommentSection from "./CommentSection/CommentSection";
import Image from '../UI/Image/Image';
import NutritionalValues from "./NutritionValues/NutritionValues";
import Auxiliary from '../../Utils/HOC/Auxiliary/Auxiliary';
import axios from 'axios';
import { server } from '../../Utils/utils';

class RecipeScreen extends React.Component {
    state = {
        ingredients: null
    }

    async componentDidMount() {
        const transformIngredients = await this.transformIngredients(this.props.ingredients);
        this.setState({ ingredients: transformIngredients });
    }

    async componentDidUpdate(prevProps, prevState) {
        if (prevProps.ingredients !== this.props.ingredients) {
            const transformIngredients = await this.transformIngredients(this.props.ingredients);
            this.setState({ ingredients: transformIngredients });
        }
    }

    transformIngredients = async (ingredients) => {
        let transformedIngredients = [];
        await axios.post(server.recipeRequests.getIngredientsForRecipe, { id: this.props._id })
            .then(response => {
                ingredients.forEach((item) => {
                    const ingredientName = response.data.find(el => el._id === item.ingredientId).name;
                    item.name = ingredientName;
                    item.unit = item.unit.toLowerCase().replace('_', ' ');
                    transformedIngredients.push(item);
                });

            })
        return transformedIngredients;
    }

    render() {
        const recipeScreen = (
            <div className={styles.RecipeScreenMain}>
                <RecipeHeader title={this.props.name} rating={this.props.rank} recipeID={this.props._id} />
                <div className={styles.RecipeScreenMainArea}>
                    <div className={styles.RecipeScreenControlArea}>
                        <Image cssClass="RecipeScreenImage" picture={this.props.picture} />
                        <CommentSection className={styles.CommentSection} comments={this.props.comments} recipeID={this.props._id} />
                    </div>
                    <div className={styles.RecipeScreenInfoArea}>
                        <div className={styles.RecipeScreenIngredientsInfo}>
                            <p style={{ 'fontWeight': "bold", 'fontSize': 20 }}>Ingredients:</p>
                            <div className={styles.RecipeScreenIngredients}>
                                <ul>
                                    {this.props.ingredients.map((ingredient, index) => (
                                        <li key={`${this.props.id}.${index}`}>
                                            {`${ingredient.amount > 0 ? ingredient.amount : ''} ${ingredient.unit === 'unit' ? '' : ingredient.unit}${ingredient.unit > 1 ? 's' : ''} of ${ingredient.name}`}
                                        </li>
                                    ))}
                                </ul>
                            </div>
                            <NutritionalValues values={this.props.nutritionValues} />
                        </div>
                        <p style={{ 'fontWeight': "bold", 'fontSize': 20 }}>How to:</p>
                        <div className={styles.RecipeScreenInstructions}>{this.props.instructions}</div>
                    </div>
                </div>
            </div>
        );
        return (
            <Auxiliary>
                {recipeScreen}
            </Auxiliary>
        );
    }
}

export default RecipeScreen;