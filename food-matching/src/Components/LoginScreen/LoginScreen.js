import React, { Component } from 'react';
import { Form, FormGroup, FormControl, Button, ButtonToolbar, Message } from 'rsuite';
import { loginModel } from '../../Utils/formValidations';
import styles from './LoginScreen.module.css';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
import Image from '../UI/Image/Image';
import googleIcon from '../../assets/google-icon.png';
import { connect } from 'react-redux';
import * as actions from '../../store/actions';

class LoginScreen extends Component {
    state = {
        formValue: {
            username: '',
            password: ''
        },
        formError: {},
        duringLogin: false
    }

    loginHandler = () => {
        const { formValue } = this.state;
        if (!this.form.check()) {
            return;
        }
        this.setState({ duringLogin: true });
        this.props.onAuthenticationStart(formValue.username, formValue.password);
    }

    static getDerivedStateFromProps(props, state) {
        if (state.duringLogin) {
            if (props.isLoggedIn) {
                props.onHideLoginModal();
            }
        }
        return null;
    }

    render() {
        const { formValue } = this.state;
        const errorMessageStyle = {
            display: this.props.loginFailed ? 'inherit' : 'none',
            paddingBottom: this.props.loginFailed ? '34px' : 0
        }

        return (
            <div className={styles.LoginContainer}>
                <Form
                    ref={ref => (this.form = ref)}
                    formValue={formValue}
                    onChange={formValue => this.setState({ formValue })}
                    onCheck={formError => this.setState({ formError })}
                    model={loginModel}
                    checkTrigger="none">
                    <div style={errorMessageStyle}>
                        <Message
                            key={this.props.loginFailed}
                            closable
                            showIcon
                            type="error"
                            title="Error"
                            description="Incorrect username or password."
                            onClose={() => this.props.onAuthenticationReset()}
                        />
                    </div>
                    <FormGroup>
                        <FormControl name="username" placeholder="Username" />
                    </FormGroup>
                    <FormGroup>
                        <FormControl name="password" type="password" placeholder="Password" />
                    </FormGroup>
                    <FormGroup>
                        <ButtonToolbar style={{ display: 'inline-flex' }}>
                            <div>
                                OR Login with:
                            </div>
                            <GoogleLogin
                                clientId=""
                                buttonText=""
                                render={renderProps => (
                                    <button className={styles.GoogleBtn}>
                                        <Image imgURL={googleIcon} innerCssClass="InnerGoogleIcon" cssClass="GoogleIcon" />
                                    </button>
                                )}
                                // onSuccess={responseGoogle}
                                // onFailure={responseGoogle}
                                cookiePolicy={'single_host_origin'}
                            />
                            <FacebookLogin
                                appId=""
                                autoLoad={true}
                                fields="name,email,picture"
                                icon="fa-facebook"
                                size="medium"
                                textButton=""
                                cssClass={styles.FacebookBtn}
                            // onClick={componentClicked}
                            // callback={responseFacebook}
                            />
                        </ButtonToolbar>
                        <Button appearance="link" onClick={this.props.switch}>Don't have an account yet? Click here!</Button>
                    </FormGroup>
                    <FormGroup>
                        <ButtonToolbar>
                            <Button appearance="primary" onClick={() => this.loginHandler()}>Login</Button>
                            <Button appearance="default" onClick={this.props.cancelled}>Cancel</Button>
                        </ButtonToolbar>
                    </FormGroup>
                </Form>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        isLoggedIn: state.users.isLoggedIn,
        loginFailed: state.users.loginFailed
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuthenticationStart: (username, password) => dispatch(actions.authenticationStart(username, password)),
        onHideLoginModal: () => dispatch(actions.hideLoginModal()),
        onAuthenticationReset: () => dispatch(actions.authenticationReset())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)