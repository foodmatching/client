import React, { Component } from 'react';
import { Card, CardImgOverlay, CardTitle } from 'reactstrap';
import Image from '../../UI/Image/Image';
import { foodCategories } from '../../../Utils/formValidations';
import styles from './FoodTypesSelector.module.css';

class FoodTypeSelector extends Component {
    render() {
        let foodTypes = null
        foodTypes = foodCategories.map((el, index) => {
            return (
                <div className={styles.PanelContainerCard} key={index}>
                    <button onClick={() => this.props.foodTypeClicked(el.value)}>
                        <Card inverse>
                            <Image innerCssClass={this.props.selectedFoodTypes.indexOf(el.value) !== -1 ? "SelectorImageSelected" : "SelectorImage"} imgURL={el.img} />
                            <CardImgOverlay style={{ padding: 0 }}>
                                <CardTitle>{el.value}</CardTitle>
                            </CardImgOverlay>
                        </Card>
                    </button>
                </div>
            )
        });

        return foodTypes;
    }
}

export default FoodTypeSelector;