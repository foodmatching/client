import React from 'react';
import { Dimmer, Loader } from 'semantic-ui-react'

const LoaderComponent = () => {
    return (
        <Dimmer active>
            <Loader size='massive' indeterminate>Loading...</Loader>
        </Dimmer>
    )
}

export default LoaderComponent
