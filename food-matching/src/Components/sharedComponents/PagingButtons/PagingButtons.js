import React from 'react';
import { Button, Icon } from 'semantic-ui-react';

const PagingButtons = (props) => {
    return (
        <div style={{ textAlign: 'center' }}>
            <Button
                inverted color='blue'
                disabled={props.prevDisabled}
                onClick={() => props.prev()}
                size='medium' animated>
                <Button.Content visible>Previous page</Button.Content>
                <Button.Content hidden>
                    <Icon name='arrow left' />
                </Button.Content>
            </Button>
            <Button
                inverted color='blue'
                disabled={props.nextDisabled}
                onClick={() => props.next()}
                size='medium' animated>
                <Button.Content visible>Next page</Button.Content>
                <Button.Content hidden>
                    <Icon name='arrow right' />
                </Button.Content>
            </Button>
        </div>
    )
}

export default PagingButtons
