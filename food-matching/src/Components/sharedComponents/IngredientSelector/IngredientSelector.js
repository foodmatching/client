import React, { Component } from 'react';
import { server } from '../../../Utils/utils';
import axios from 'axios';
import styles from './IngredientSelector.module.css';
import { Card, CardTitle, CardImgOverlay } from 'reactstrap';
import Image from '../../UI/Image/Image';
import thumbsUp from '../../../assets/thumbs-up.png';
import thumbsDown from '../../../assets/thumbs-down.png';
import { healthIssuesDataList } from '../../../Utils/formValidations';

class IngredientSelector extends Component {
    state = {
        allIngredientsList: null
    }

    componentDidMount() {
        axios.post(server.ingredientRequests.getAllIngredients, { size: 100 })
            .then(response => {
                this.setState({ allIngredientsList: response.data })
            })
            .catch(console.log);
    }

    checkGoodForHealth = (ingredient) => {
        let good = false;
        let bad = false;
        if (ingredient) {
            if (ingredient.goodFor.length > 0) {
                for (let i = 0; i < ingredient.goodFor.length; i++) {
                    for (let k = 0; k < this.props.usersHealthIssues.length; k++) {
                        for (let j = 0, found = false; j < healthIssuesDataList.length && !found; j++) {
                            if (healthIssuesDataList[j].value.toLowerCase() === this.props.usersHealthIssues[k].toLowerCase() &&
                                healthIssuesDataList[j].value.toLowerCase() === ingredient.goodFor[i].toLowerCase()) {
                                good = true;
                                found = true;
                            }
                        }
                    }
                }
            }

            if (ingredient.badFor.length > 0) {
                for (let i = 0; i < ingredient.badFor.length; i++) {
                    for (let k = 0; k < this.props.usersHealthIssues.length; k++) {
                        for (let j = 0, found = false; j < healthIssuesDataList.length && !found; j++) {
                            if (healthIssuesDataList[j].value.toLowerCase() === this.props.usersHealthIssues[k].toLowerCase() &&
                                healthIssuesDataList[j].value.toLowerCase() === ingredient.badFor[i].toLowerCase()) {
                                bad = true;
                                found = true;
                            }
                        }
                    }
                }
            }
        }

        if (good === bad) {
            return null;
        }

        if (good) {
            return (<Image imgURL={thumbsUp} innerCssClass="ThumbImage" cssClass="Thumb" />);
        } else {
            return (<Image imgURL={thumbsDown} innerCssClass="ThumbImage" cssClass="Thumb" />);
        }
    }

    render() {
        let ingredients = null;
        if (this.state.allIngredientsList) {
            ingredients = this.state.allIngredientsList.map(el => {
                return (
                    <div className={styles.PanelContainerCard} key={el._id}>
                        <button onClick={() => this.props.ingredientClicked(el._id)}>
                            <Card inverse>
                                <Image
                                    innerCssClass={this.props.selectedIngredients.indexOf(el._id) !== -1 ? "SelectorImageSelected" : "SelectorImage"} imgURL={el.img}
                                    picture={el.picture} />
                                <CardImgOverlay style={{ padding: 0 }}>
                                    <CardTitle>{el.name}</CardTitle>
                                    {this.checkGoodForHealth(el)}
                                </CardImgOverlay>
                            </Card>
                        </button>
                    </div>
                )
            });
        }
        return ingredients
    }
}

export default IngredientSelector