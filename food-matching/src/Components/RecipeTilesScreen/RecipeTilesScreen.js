import React, { Component } from "react";
import RecipeTile from "./RecipeTile/RecipeTile";
import "./RecipeTilesScreen.css";
import { connect } from 'react-redux'
import * as actions from '../../store/actions'
import { withRouter } from 'react-router-dom';
import PagingButtons from '../sharedComponents/PagingButtons/PagingButtons';
import LoaderComponent from '../sharedComponents/LoaderComponent/LoaderComponent';
import { Segment } from 'semantic-ui-react';

class RecipeTilesScreen extends Component {
    state = {
        page: 1,
        size: 18
    }

    componentDidMount() {
        this.setRecipeList();
    }

    componentDidUpdate(prevProps, prevState) {
        if ((prevState.page !== this.state.page) ||
            (prevProps.user !== this.props.user)) {
            this.setRecipeList();
        }
    }

    nextPageHandler = () => {
        this.setState(prevState => {
            return { page: prevState.page + 1 }
        })
    }

    prevPageHandler = () => {
        this.setState(prevState => {
            return { page: prevState.page - 1 }
        })
    }

    setRecipeList = () => {
        const { page, size } = this.state;
        const currentPath = this.props.match.path;
        let personalizedFeed = true;
        if (this.props.user) {
            if (currentPath === '/user/saved-recipes') {
                personalizedFeed = false; //display saved recipes
            }
            this.props.onResetList();
            this.props.onInitRecipeList(page, size, this.props.user._id, personalizedFeed);
        } else {
            this.props.onInitRecipeList(page, size);
        }
    }


    render() {
        let recipeTiles = null;
        const loader = <LoaderComponent />;
        let numberOfDisplayedRecipes = 0;
        if (this.props.recipes) {
            numberOfDisplayedRecipes = this.props.recipes.length;
            recipeTiles = (
                <div className="RecipeTilesScreenContainer">
                    {
                        this.props.recipes.map((recipe) => {
                            return (
                                <RecipeTile
                                    key={recipe._id}
                                    id={recipe._id}
                                    rank={recipe.rank}
                                    recipeTitle={recipe.name}
                                    picture={recipe.picture}
                                    ingredientList={recipe.ingredients} />
                            )
                        })}
                </div>
            )
        }
        return (
            <Segment>
                <div style={{ width: '1-webkit-fill-available', height: '800px' }}>
                    {this.props.loading ? loader : null}
                    {recipeTiles}
                    <PagingButtons
                        next={this.nextPageHandler}
                        prev={this.prevPageHandler}
                        nextDisabled={numberOfDisplayedRecipes < this.state.size}
                        prevDisabled={this.state.page === 1} />

                </div>
            </Segment>
        )
    }
}

const mapStateToProps = state => {
    return {
        recipes: state.recipes.recipeList,
        loading: state.recipes.loading,
        newRecipeUploadSuccess: state.recipes.newRecipeUploadSuccess,
        user: state.users.user
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onInitRecipeList: (page, size, userID, personalizedFeed) => dispatch(actions.initRecipeList(page, size, userID, personalizedFeed)),
        onResetList: () => dispatch(actions.resetRecipeList())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(RecipeTilesScreen));