import React, { Component } from "react";
import "./RecipeTile.css";
import { NavLink } from "react-router-dom";
import Image from '../../UI/Image/Image';
import { Rating } from 'semantic-ui-react';

class RecipeTile extends Component {
    render() {
        let rank = this.props.rank > 0 ? this.props.rank : 0;
        const rating = (
            <Rating
                icon='heart'
                disabled
                size='massive'
                rating={rank}
                maxRating={5} />
        );
        return (
            <NavLink to={`recipe/${this.props.id}`} className="RecipeTileContainer">
                <Image cssClass="RecipeTileImage" picture={this.props.picture} />
                <div className="RecipeTilePreview">
                    <h5>
                        {this.props.recipeTitle}
                    </h5>
                    {rating}
                </div>
            </NavLink>
        )
    }
}

export default RecipeTile