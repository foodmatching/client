import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { server, getPictureSrc } from '../../../../Utils/utils';
import { connect } from 'react-redux';
import { Card, Rating, Button, Icon } from 'semantic-ui-react';
import Image from '../../../UI/Image/Image';
import PagingButtons from '../../../sharedComponents/PagingButtons/PagingButtons';

class SingleCookBook extends Component {
    state = {
        recipes: null,
        error: '',
        size: 18,
        page: 1
    }

    componentDidMount() {
        //get recipes
        this.setRecipeList();
    }

    componentDidUpdate(prevProps, prevState) {
        if ((prevState.page !== this.state.page) ||
            (prevProps.user !== this.props.user)) {
            this.setRecipeList();
        }
    }


    setRecipeList = () => {
        const { page, size } = this.state;
        const cookBookName = this.props.match.params.cookBookName;
        const userID = this.props.user._id
        if (cookBookName && userID) {
            axios.post(server.userRequests.getAllRecipesInCookBook, { id: userID, name: cookBookName, pageNo: page, size: size })
                .then(response => {
                    this.setState({ recipes: response.data, error: '' });
                })
                .catch(() => this.setState({ error: 'The recipe couldn\'t be retrieved from the server.' }));
        }
    }

    gotoRecipe = (event, data) => {
        this.props.history.push(`${this.props.history.location.pathname}/recipe/${data.recipe_id}`);
    }

    goBackHandler = () => {
        this.props.history.goBack();
    }

    nextPageHandler = () => {
        this.setState(prevState => {
            return { page: prevState.page + 1 }
        })
    }

    prevPageHandler = () => {
        this.setState(prevState => {
            return { page: prevState.page - 1 }
        })
    }

    render() {
        let numberOfDisplayedRecipes = 0;
        let recipeCards = null;
        if (this.state.recipes) {
            numberOfDisplayedRecipes = this.state.recipes.length;
            recipeCards = this.state.recipes.map(recipe => {
                let rating = 'Unrated';
                if (recipe.recipe.rank > 0) {
                    rating = (
                        <Rating
                            icon='heart'
                            disabled
                            size='massive'
                            rating={recipe.recipe.rank}
                            maxRating={5} />
                    );
                }

                let image = 'https://www.gannett-cdn.com/-mm-/1fecae5856e58374cc9e1c0fd6dcc3c6aae79d4e/c=0-293-5760-3547/local/-/media/2018/07/17/IAGroup/DesMoines/636674359927753055-0717-NEW-STATEFAIR-FOODS-00029.jpg?width=3200&height=1680&fit=crop';
                if (recipe.recipe.picture) {
                    image = getPictureSrc(recipe.recipe.picture.data.data);
                }

                const img = <Image imgURL={image} cssClass="RecipeTileImage" />

                return (
                    <Card
                        recipe_id={recipe.recipe._id}
                        key={recipe.recipe._id}
                        centered
                        raised
                        image={img}
                        header={recipe.recipe.name}
                        description={rating}
                        onClick={this.gotoRecipe}
                    />
                )
            });
        }

        const headerStyle = {
            textAlign: 'center',
            fontSize: '50px',
            color: '#ff8300',
            textShadow: '1px -1px 3px #6f6a6a'
        }

        return (
            <div>
                <div style={{ position: 'absolute', top: 0 }}>
                    <Button
                        onClick={this.goBackHandler}
                        size='large' animated>
                        <Button.Content visible>Back</Button.Content>
                        <Button.Content hidden>
                            <Icon name='arrow left' />
                        </Button.Content>
                    </Button>
                </div>
                <h1 style={headerStyle}>{this.props.match.params.cookBookName}</h1>
                <Card.Group centered itemsPerRow={5}>
                    {recipeCards}
                </Card.Group>
                <div style={{ paddingTop: '20px' }}>
                    <PagingButtons
                        next={this.nextPageHandler}
                        prev={this.prevPageHandler}
                        nextDisabled={numberOfDisplayedRecipes < this.state.size}
                        prevDisabled={this.state.page === 1} />
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        user: state.users.user,
        loading: state.recipes.loading
    }
}
export default connect(mapStateToProps)(withRouter(SingleCookBook))