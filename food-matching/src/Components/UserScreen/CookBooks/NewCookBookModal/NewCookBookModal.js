import React, { Component } from 'react';
import { Modal, Form, FormGroup, ControlLabel, FormControl, Button, ButtonToolbar } from 'rsuite';
import { newCookBook } from '../../../../Utils/formValidations';
import { connect } from 'react-redux';
import * as actions from '../../../../store/actions';

class NewCookBookModal extends Component {
    state = {
        formValue: {
            name: '',
            nameList: []
        },
        formError: {}
    }

    componentDidMount() {
        const updatedFormValue = { ...this.state.formValue };
        updatedFormValue.nameList = this.props.bookNameList
        this.setState({ formValue: updatedFormValue });
    }

    checkValidationHandler = () => {
        if (!this.form.check()) {
            return;
        }
        this.props.onAddCookBook(this.props.userID, this.state.formValue.name);
    }

    render() {
        const { formValue } = this.state;

        return (
            <Modal show onHide={this.props.onClose} size="xs">
                <Modal.Header>
                    <Modal.Title>Add another book to your collection</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form
                        checkTrigger='none'
                        ref={ref => (this.form = ref)}
                        onChange={formValue => { this.setState({ formValue }); }}
                        onCheck={formError => { this.setState({ formError }); }}
                        formValue={formValue}
                        fluid
                        model={newCookBook}>
                        <FormGroup>
                            <ControlLabel>Enter a name for your new cook book</ControlLabel>
                            <FormControl name="name" />
                        </FormGroup>
                        <FormGroup>
                            <ButtonToolbar>
                                <Button onClick={() => this.checkValidationHandler()} appearance="primary">Create</Button>
                                <Button onClick={this.props.onClose} appearance="subtle">Cancel</Button>
                            </ButtonToolbar>
                        </FormGroup>
                    </Form>
                </Modal.Body>
            </Modal>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAddCookBook: (userID, bookName) => dispatch(actions.addNewCookBook(userID, bookName))
    }
}

const mapStateToProps = state => {
    return {
        addBookFailed: state.users.addBookFailed
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewCookBookModal);