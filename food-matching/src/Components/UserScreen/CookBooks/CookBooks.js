import React, { Component } from 'react';
import { Card } from 'semantic-ui-react';
import addBookIcon from '../../../assets/add.png';
import bookIcon from '../../../assets/book-icon.png';
import NewCookBookModal from './NewCookBookModal/NewCookBookModal';
import Auxiliary from '../../../Utils/HOC/Auxiliary/Auxiliary';
import { connect } from 'react-redux'

class CookBooks extends Component {
    state = {
        showModal: false,
        bookNameList: []
    }

    static getDerivedStateFromProps(props, state) {
        if (props.user.cookBooks.length !== state.bookNameList.length) {
            const nameList = props.user.cookBooks.map(el => el.name);
            return {
                ...state,
                showModal: false,
                bookNameList: [...nameList]
            }
        }
        return null;
    }

    openModal = () => {
        this.setState({ showModal: true })
    }

    closeModal = () => {
        this.setState({ showModal: false })
    }

    renderSingleBook = (event, data) => {
        this.props.history.push(`/user/cookbooks/${data.header}`);
    }

    render() {
        const newBookCard = (
            <Card
                onClick={() => this.openModal()}
                centered
                raised
                image={addBookIcon}
                header='New Book'
            />
        );

        const cookBookCards = this.props.user.cookBooks.map(book => {
            return (
                <Card
                    key={book._id}
                    centered
                    raised
                    image={bookIcon}
                    header={book.name}
                    meta={`${book.recipes.length} Recipes`}
                    onClick={this.renderSingleBook}
                />
            )
        });

        return (
            <Auxiliary>
                {
                    this.state.showModal ?
                        (
                            <NewCookBookModal
                                userID={this.props.user._id}
                                onClose={this.closeModal}
                                bookNameList={this.state.bookNameList} />
                        ) : null
                }
                <div style={{ padding: '0.5%' }}>
                    <Card.Group itemsPerRow={6}>
                        {newBookCard}
                        {cookBookCards}
                    </Card.Group>
                </div>
            </Auxiliary>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user
    }
}

export default connect(mapStateToProps)(CookBooks)
