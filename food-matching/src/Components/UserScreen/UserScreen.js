import React, { Component } from 'react';
import { Tab } from 'semantic-ui-react';
import styles from './UserScreen.module.css';
import CookBooks from './CookBooks/CookBooks';
import SingleCookeBook from './CookBooks/SingleCookBook/SingleCookBook';
import { withRouter, Route, Switch } from 'react-router-dom';
import RecipeScreenContainer from '../../containers/RecipeScreenContainer/RecipeScreenContainer';
import RecipeTileScreen from '../RecipeTilesScreen/RecipeTilesScreen';

class UserScreen extends Component {

    componentDidMount() {
        // default page is cookbooks
        this.props.history.replace('/user/cookbooks')
    }

    handleTabChange = (event, data) => {
        data.activeIndex === 1 ?
            this.props.history.replace('/user/saved-recipes') :
            this.props.history.replace('/user/cookbooks')
    }


    render() {
        const paneStyle = {
            border: 'none',
            boxShadow: 'none'
        }

        const panes = [
            {
                menuItem: 'Cook Books', render: () => (
                    <Tab.Pane style={paneStyle} attached={false}>
                        <Switch>
                            <Route exact path='/user/cookbooks' component={CookBooks} />
                            <Route exact path='/user/cookbooks/:cookBookName' component={SingleCookeBook} />
                            <Route exact path='/user/cookbooks/:cookBookName/recipe/:recipeId' component={RecipeScreenContainer} />
                        </Switch>
                    </Tab.Pane>
                )
            },
            {
                menuItem: 'Saved Recipes', render: () => (
                    <Tab.Pane style={paneStyle} attached={false}>
                        <Route exact path='/user/saved-recipes' component={RecipeTileScreen} />
                        <Route exact path='/user/recipe/:recipeId' component={RecipeScreenContainer} />
                    </Tab.Pane>
                )
            }
        ]
        return (
            <div className={styles.TabContainer}>
                <Tab
                    activeIndex={this.props.history.location.pathname.includes('/user/cookbooks') ? 0 : 1}
                    onTabChange={this.handleTabChange}
                    menu={{ color: 'blue', secondary: true, pointing: true }}
                    panes={panes} />
            </div>
        )
    }
}


export default withRouter(UserScreen);