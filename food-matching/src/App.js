import React, { Component } from 'react';
import MainScreen from './containers/MainScreen/MainScreen';
import NavBar from './components/NavBar/NavBar';
import { connect } from 'react-redux';
import * as actions from './store/actions';
import ModalContainer from './components/UI/ModalContainer/ModalContainer';
import NewRecipeScreen from './components/NewRecipeScreen/NewRecipeScreen';

class App extends Component {
  componentDidMount() {
    // this.props.onConnect('Amit', '1234');
    // this.props.onConnect('avigo', 'a');
  }


  render() {
    return (
      <div className="App">
        <NavBar
          isLoggedIn={this.props.isLoggedIn}
          user={this.props.user} />
        <MainScreen />
        {this.props.showRegistration || this.props.showLogin ? <ModalContainer /> : null}
        {this.props.showNewRecipe ? <NewRecipeScreen /> : null}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.users.isLoggedIn,
    user: state.users.user,
    showNewRecipe: state.recipes.showNewRecipeModal,
    showRegistration: state.users.showRegistration,
    showLogin: state.users.showLogin
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onConnect: (username, password) => dispatch(actions.authenticationStart(username, password))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
