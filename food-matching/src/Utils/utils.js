
export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    }
}

export const imgURL = 'https://www.gannett-cdn.com/-mm-/1fecae5856e58374cc9e1c0fd6dcc3c6aae79d4e/c=0-293-5760-3547/local/-/media/2018/07/17/IAGroup/DesMoines/636674359927753055-0717-NEW-STATEFAIR-FOODS-00029.jpg?width=3200&height=1680&fit=crop';

export const deepCloneObject = (obj) => {
    return JSON.parse(JSON.stringify(obj));
}

export const getTimeString = (time) => {
    const date = time ? new Date(time) : new Date();
    const dateString = date.toISOString().slice(0, 16).replace('T', ', ');
    return dateString;
}

export const getPictureSrc = (binaryImage) => {
    const base64Flag = 'data:image/jpeg;base64,';
    const imageStr = arrayBufferToBase64(binaryImage);
    return base64Flag + imageStr;
}

export const unitConversion = unitID => {
    switch (unitID) {
        case 0: return 'table spoon';
        case 1: return 'tea spoon';
        case 2: return 'cup';
        case 3: return 'ml';
        case 4: return 'mg';
        case 5: return 'g';
        default: return 'unit';
    }
}

export const arrayBufferToBase64 = (buffer) => {
    var binary = '';
    var bytes = [].slice.call(new Uint8Array(buffer));
    bytes.forEach((b) => binary += String.fromCharCode(b));
    return window.btoa(binary);
};

/* 
Splits the data received from a form to outer and inner data.
data{
    property1,
    property2,
    property3.property3.1
    property3.property3.2
}

becomes:
data{
    outer:{
        property1,
        property2
    },
    inner:{
        property3.1
        property3.2
    }
}
*/
export const fixDataStructure = data => {
    let innerData, outerData;

    const dataToArr = Object.entries(data);
    dataToArr.forEach(entry => {
        let key = entry[0];
        const value = entry[1];

        if (key.includes('profile.')) {
            key = key.slice(8);
            innerData = {
                ...innerData,
                [key]: value
            }
        } else if (!key.includes('verify') && !key.startsWith('is', 0) && key !== 'allIngredientsList') {
            outerData = {
                ...outerData,
                [key]: value
            }
        }
    })

    return {
        outerData: outerData,
        innerData: innerData
    }
}

const recipe = 'http://193.106.55.184:3000/Recipe'
const ingredient = 'http://193.106.55.184:3000/Ingredient'
const user = 'http://193.106.55.184:3000/User'

export const server = {
    recipeRequests: {
        addRecipe: `${recipe}/AddRecipe`,
        addRecipes: `${recipe}/AddRecipes`,
        getAllRecipes: `${recipe}/GetAllRecipes`,
        GetRecipeByName: `${recipe}/GetRecipeByName`,
        GetRecipeByID: `${recipe}/GetRecipeByID`,
        deleteRecipe: `${recipe}/DeleteRecipe`,
        deleteRecipeByName: `${recipe}/DeleteRecipeByName`,
        addComment: `${recipe}/AddComment`,
        getAllCommentsForRecipe: `${recipe}/getAllCommentsForRecipe`,
        getIngredientsForRecipe: `${recipe}/getIngredientsForRecipe`,
        addRecipePicture: `${recipe}/addRecipePicture`,
        addRecipeCommentPicture: `${recipe}/addRecipeCommentPicture`,
        GetRecipesContainingNamesAndIds: `${recipe}/GetRecipesContainingNamesAndIds`,
        GetRecipesContaining: `${recipe}/GetRecipesContaining`
    },
    ingredientRequests: {
        addIngredient: `${ingredient}/AddIngredient`,
        addIngredients: `${ingredient}/AddIngredients`,
        getAllIngredients: `${ingredient}/GetAllIngredients`,
        GetIngredientByName: `${ingredient}/GetIngredientByName`,
        GetIngredientByID: `${ingredient}/GetIngredientByID`,
        GetIngredientStartWith: `${ingredient}/GetIngredientStartWith`,
        deleteIngredient: `${ingredient}/DeleteIngredient`,
        deleteIngredientByName: `${ingredient}/DeleteIngredientByName`,
        addBadFor: `${ingredient}/addBadFor`,
        addGoodFor: `${ingredient}/addGoodFor`,
        addIngredientPicture: `${ingredient}/addIngredientPicture`
    },
    userRequests: {
        connect: `${user}/Connect`,
        addNewUser: `${user}/AddNewUser`,
        deleteUserByUserName: `${user}/DeleteUserByUserName`,
        deleteUserById: `${user}/DeleteUserById`,
        getAllUsers: `${user}/GetAllUsers`,
        getUserByID: `${user}/getUserByID`,
        getUserNameAndPictureByID: `${user}/getUserNameAndPictureByID`,
        getUserByFacebook: `${user}/getUserByFacebook`,
        getUserByGmail: `${user}/getUserByGmail`,
        getUserByUserName: `${user}/getUserByUserName`,
        addHealthIssues: `${user}/addHealthIssues`,
        addIngredientsIDsLikes: `${user}/addIngredientsIDsLikes`,
        addTypesLikes: `${user}/addTypesLikes`,
        addUserPicture: `${user}/addUserPicture`,
        addCookBook: `${user}/addCookBook`,
        addRecipeToCookBook: `${user}/addRecipeToCookBook`,
        getAllCookBooks: `${user}/getAllCookBooks`,
        getAllRecipesInCookBook: `${user}/getAllRecipesInCookBook`,
        getAllUserRecipes: `${user}/getAllUserRecipes`,
        getRecipesMatchForUser: `${user}/getRecipesMatchForUser`,
        isRecipeRecommendedForUser: `${user}/isRecipeRecommendedForUser`,
        isIngredientRecommendedForUser: `${user}/isIngredientRecommendedForUser`
    }
}
