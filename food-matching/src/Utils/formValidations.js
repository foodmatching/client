import { Schema } from 'rsuite';
//_______________________________General______________________________
export const healthIssueConversion = list => {
    const convertedIssues = [];
    healthIssuesDataList.forEach(issue => {
        if (list.indexOf(issue.value) >= 0) {
            convertedIssues.push(issue.numericalValue);
        }
    });
    return convertedIssues;
}

export const healthIssuesDataList = [
    {
        label: 'Celiac',
        value: 'Celiac',
        role: 'master',
        numericalValue: -1
    },
    {
        label: 'Gluten Intolerance',
        value: 'Gluten Intolerance',
        role: 'master',
        numericalValue: 2
    },
    {
        label: 'Diabetes',
        value: 'Diabetes',
        role: 'master',
        numericalValue: 1
    },
    {
        label: 'Lactose intolerance',
        value: 'Lactose intolerance',
        role: 'master',
        numericalValue: 0
    },
    {
        label: 'Vegetarian',
        value: 'Vegetarian',
        role: 'master',
        numericalValue: 3
    },
    {
        label: 'Vegan',
        value: 'Vegan',
        role: 'master',
        numericalValue: 4
    },
    {
        label: 'High Cholesterol',
        value: 'High Cholesterol',
        role: 'master',
        numericalValue: 5
    }
];

// _______________________________Registration______________________________

const { StringType, NumberType, DateType } = Schema.Types;

//_________ Screen 1________
export const firstScreenModel = Schema.Model({
    username: StringType()
        .minLength(4, 'The username is too short')
        .maxLength(8, 'The username is too long')
        .addRule((value, data) => !data.isUsernameAvailable ? false : true, 'This username is already taken.')
        .isRequired('This field is required.'),
    password: StringType()
        .minLength(8, 'The password is too short')
        .maxLength(16, 'The password is too long')
        .isRequired('This field is required.'),
    verifyPassword: StringType()
        .addRule((value, data) => value !== data.password ? false : true, 'The two passwords do not match.')
        .isRequired('This field is required.'),
    email: StringType().isEmail().isRequired('This field is required.'),
    'profile.name': StringType()
        .minLength(2, 'A name cannot be this short.')
        .addRule(value => value.match(/^[A-Za-z ]+$/) ? true : false, 'A name can contains letters only.')
        .isRequired('This field is required.')

});

//_________ Screen 3________
export const thirdScreenModel = Schema.Model({
    'profile.height': NumberType()
        .isRequired('This field cannot be empty.')
        .range(54.64, 280, 'Please enter a valid height.'),
    'profile.weight': NumberType()
        .range(2.1, 650, 'Please enter a valid weight.')
        .isRequired('This field cannot be empty.'),
    'profile.dateOfBirth': DateType()
        .isRequired('This field cannot be empty.')
        .range('1890-01-01', '2009-01-01', 'Please pick a valid date.')
});

//_________ Screen 4________
export const foodCategories = [
    {
        value: 'American',
        img: 'https://www.bbcgoodfood.com/sites/default/files/recipe-collections/collection-image/2013/05/buffalo-wings.jpg'
    },
    {
        value: 'British',
        img: 'https://www.bbcgoodfood.com/sites/default/files/Collections_British_4.jpg'
    }, {
        value: 'Caribbean',
        img: 'https://www.bbcgoodfood.com/sites/default/files/rum-punch.jpg'
    },
    {
        value: 'Chinese',
        img: 'https://www.bbcgoodfood.com/sites/default/files/recipe-collections/collection-image/2013/05/chinese-beef-aubergine-hotpot.jpg'
    },
    {
        value: 'French',
        img: 'https://www.bbcgoodfood.com/sites/default/files/recipe-collections/collection-image/2013/05/blood-orange-madeleines.jpg'
    },
    {
        value: 'Greek',
        img: 'https://www.bbcgoodfood.com/sites/default/files/kleftiko.jpg'
    },
    {
        value: 'Indian',
        img: 'https://www.bbcgoodfood.com/sites/default/files/Collections_Indian_4.jpg'
    },
    {
        value: 'Italian',
        img: 'https://www.bbcgoodfood.com/sites/default/files/recipe-collections/collection-image/2013/05/canoli3.jpg'
    },
    {
        value: 'Japanese',
        img: 'https://www.bbcgoodfood.com/sites/default/files/recipe-collections/collection-image/2013/05/ramen.jpg'
    },
    {
        value: 'Mediterranean',
        img: 'https://www.bbcgoodfood.com/sites/default/files/2196639_MEDIUM.jpeg'
    },
    {
        value: 'Mexican',
        img: 'https://www.bbcgoodfood.com/sites/default/files/styles/recipe/public/recipe/recipe-image/2018/06/breakfast-burrito.jpg?itok=jCKa1v-x'
    },
    {
        value: 'Moroccan',
        img: 'https://www.bbcgoodfood.com/sites/default/files/tagine_1.jpg'
    },
    {
        value: 'Spanish',
        img: 'https://www.bbcgoodfood.com/sites/default/files/2138651_MEDIUM.jpeg'
    },
    {
        value: 'Thai',
        img: 'https://www.bbcgoodfood.com/sites/default/files/2991673_MEDIUM-1.jpeg'
    },
    {
        value: 'Turkish',
        img: 'https://www.bbcgoodfood.com/sites/default/files/681666_MEDIUM.jpeg'
    },
    {
        value: 'Vietnamese',
        img: 'https://www.bbcgoodfood.com/sites/default/files/caramel-trout.jpg'
    }
]


// _______________________________Login______________________________
export const loginModel = Schema.Model({
    username: StringType()
        .isRequired('This field is required.'),
    password: StringType()
        .isRequired('This field is required.')
});

//_______________________________New Cook Book______________________________
export const newCookBook = Schema.Model({
    name: StringType()
        .addRule((value, data) => !data.nameList.includes(value), 'You already have a book with this name.')
        .isRequired('This field is required.')
});

//_______________________________New Recipe______________________________

export const unitData = [
    {
        label: 'Table spoon',
        value: 'table spoon',
        role: 'Master'
    },
    {
        label: 'Tea spoon',
        value: 'tea spoon',
        role: 'Master'
    },
    {
        label: 'Cup',
        value: 'cup',
        role: 'Master'
    },
    {
        label: 'ml',
        value: 'ml',
        role: 'Master'
    },
    {
        label: 'mg',
        value: 'mg',
        role: 'Master'
    },
    {
        label: 'g',
        value: 'g',
        role: 'Master'
    },
    {
        label: 'Unit',
        value: 'unit',
        role: 'Master'
    }
];